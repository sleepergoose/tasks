﻿using Xunit;
using System;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using ProjectStructure.WebAPI.IntegrationTests.WebAppFactory;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly string _endpoint = "api/Projects";

        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        [Fact]
        public async Task Add_WhenAddProject_ThenResponseWithCode201AndCorrespondedBody()
        {
            var projectDto = new ProjectDTO
            {
                AuthorId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                Description = "Description",
                Name = "Project name",
                TeamId = 1
            };

            var projectInJson = JsonConvert.SerializeObject(projectDto);

            var httpResponse = await _client.PostAsync(_endpoint, new StringContent(projectInJson, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(projectDto.Name, createdProject.Name);
            Assert.Equal(projectDto.AuthorId, createdProject.AuthorId);
            Assert.Equal(projectDto.TeamId, createdProject.TeamId);

            await _client.DeleteAsync($"{_endpoint}/{createdProject.Id}");
        }



        [Fact]
        public async Task Add_WhenProjectDtoIsNull_ThenResponseWithCode400()
        {
            ProjectDTO projectDto = null;

            var projectInJson = JsonConvert.SerializeObject(projectDto);
            var httpResponse = await _client.PostAsync(_endpoint, new StringContent(projectInJson, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }



        [Theory]
        [InlineData(" {\"id\": 0,\"authorId\": A,\"teamId\": 1,\"name\": \"Name\",\"description\": \"Description\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"authorId\": 1,\"teamId\": 1,\"name\": \"\",\"description\": \"Description\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"authorId\": -1,\"teamId\": 1,\"name\": \"Name\",\"description\": \"Description\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"authorId\": 1,\"teamId\": -1,\"name\": \"Name\",\"description\": \"Description\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"authorId\": 1,\"teamId\": A,\"name\": \"Name\",\"description\": \"Description\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"authorId\": 1,\"teamId\": A,\"name\": \"Name\",\"description\": \"\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"authorId\": 1,\"teamId\": A,\"name\": \"Name\",\"description\": \"Description\",\"deadline\": \"\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"authorId\": 1,\"teamId\": 1,\"name\": \"Name\",\"description\": \"Description\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": null}")]
        [InlineData(" {\"id\": 0,\"authorId\": null,\"teamId\": 1,\"name\": \"Name\",\"description\": \"Description\",\"deadline\": \"2021-11-12T18:54:38.17\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        public async Task Add_WhenWrongRequestBody_ThenResponseCode400(string jsonInString)
        {
            var httpResponse = await _client.PostAsync(_endpoint, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
