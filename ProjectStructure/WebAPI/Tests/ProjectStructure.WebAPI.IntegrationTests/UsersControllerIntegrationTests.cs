﻿using Xunit;
using System;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using ProjectStructure.WebAPI.IntegrationTests.WebAppFactory;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly string _endpoint = "api/Users";


        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task Delete_WhenRemoveUser_ThenThereIsNoSuchAnId(int id)
        {
            // Get user to save it in memory
            var httpResponse = await _client.GetAsync($"{_endpoint}/{id}");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<UserDTO>(stringResponse);

            // Remove user from DB
            httpResponse = await _client.DeleteAsync($"{_endpoint}/{id}");
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

            // Is there deleted user in the DB?
            httpResponse = await _client.GetAsync($"{_endpoint}/{id}");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

            // Save user to DB 
            var userInJson = JsonConvert.SerializeObject(user);
            await _client.PostAsync(_endpoint, new StringContent(userInJson, Encoding.UTF8, "application/json"));
        }



        [Theory]
        [InlineData(int.MaxValue - 1)]
        public async Task Delete_WhenThereIsNoSuchAUser_ThenStatusCode404(int id)
        {
            var httpResponse = await _client.DeleteAsync($"{_endpoint}/{id}");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }



        [Fact]
        public async Task Add_WhenAddUser_ThenStatusCode201AndUserHasCorrespondedBody()
        {
            var newUserDto = new UserDTO
            {
                BirthDay = DateTime.Now,
                Email = "user@server.com",
                FirstName = "Donald",
                LastName = "Tramp",
                RegisteredAt = DateTime.Now,
                TeamId = null,
                Id = 0
            };

            var userInJson = JsonConvert.SerializeObject(newUserDto);

            var httpResponse = await _client.PostAsync(_endpoint, new StringContent(userInJson, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);

            Assert.Equal(newUserDto.FirstName, createdUser.FirstName);
            Assert.Equal(newUserDto.LastName, createdUser.LastName);
            Assert.Equal(newUserDto.BirthDay, createdUser.BirthDay);
            Assert.Equal(newUserDto.Email, createdUser.Email);
        }
    }
}
