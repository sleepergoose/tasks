﻿using Xunit;
using System;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using ProjectStructure.WebAPI.IntegrationTests.WebAppFactory;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly string _endpoint = "api/Tasks";

        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task Delete_WhenRemoveTask_ThenThereIsNoSuchAnId(int id)
        {
            // Get task to save it in memory
            var httpResponse = await _client.GetAsync($"{_endpoint}/{id}");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var task = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            // Remove task from DB
            httpResponse = await _client.DeleteAsync($"{_endpoint}/{id}");
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

            // Is there deleted task in the DB?
            httpResponse = await _client.GetAsync($"{_endpoint}/{id}");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

            // Save task to DB 
            var taskInJson = JsonConvert.SerializeObject(task);
            await _client.PostAsync(_endpoint, new StringContent(taskInJson, Encoding.UTF8, "application/json"));
        }


        [Theory]
        [InlineData(10)]
        public async Task Update_WhenTaskUpdateAsFinished_ThenStatusCode200AndCorrespondedBody(int id)
        {
            /* Arrange */
            var httpResponse = await _client.GetAsync($"{_endpoint}/{id}");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var task = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            task.FinishedAt = DateTime.Now;
            task.State = DAL.Enums.TaskState.Done;

            // Update task
            var taskInJson = JsonConvert.SerializeObject(task);

            httpResponse = await _client.PutAsync(_endpoint, new StringContent(taskInJson, Encoding.UTF8, "application/json"));
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var updatedTask = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.True(updatedTask.FinishedAt != null);
            Assert.True(updatedTask.State == DAL.Enums.TaskState.Done);
        }


        [Theory]
        [InlineData(int.MaxValue - 1)]
        public async Task Delete_WhenThereIsNoSuchATask_ThenStatusCode404(int id)
        {
            var httpResponse = await _client.DeleteAsync($"{_endpoint}/{id}");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}
