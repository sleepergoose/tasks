﻿using Xunit;
using System;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Abstract;

namespace ProjectStructure.BLL.Tests
{
    public class UserServiceTests : BaseTestClass
    {

        private readonly UserService _userService;

        public UserServiceTests()
        {
            _userService = new UserService(_context, _mapper);
        }


        [Fact]
        public async Task AddUser_WhenAddUser_ThenThereAreTwoUsers()
        {
            var userDto = new UserDTO
            {
                Id = 2,
                Email = "user@server.com",
                FirstName = "Peter",
                LastName = "Pen",
                BirthDay = DateTime.Parse("2001-01-01T00:00:00.00"),
                RegisteredAt = DateTime.Parse("2017-01-01T00:00:00.00")
            };

            var createdUser = await _userService.AddEntityAsync(userDto);

            Assert.Equal(2, (await _userService.GetAllEntitiesAsync() as ICollection<UserDTO>).Count);

            await _userService.DeleteEntityAsync(createdUser.Id);
        }


        [Fact]
        public async Task AddUser_WhenAddUser_ThenCorespondedBody()
        {
            var userDto = new UserDTO
            {
                Id = 2,
                Email = "user@server.com",
                FirstName = "Peter",
                LastName = "Pen",
                BirthDay = DateTime.Parse("2001-01-01T00:00:00.00"),
                RegisteredAt = DateTime.Parse("2017-01-01T00:00:00.00")
            };

            var createdUser = await _userService.AddEntityAsync(userDto);

            Assert.Equal(userDto.FirstName, createdUser.FirstName);
            Assert.Equal(userDto.LastName, createdUser.LastName);
            Assert.Equal(userDto.Email, createdUser.Email);
            Assert.Equal(userDto.BirthDay, createdUser.BirthDay);
            Assert.Equal(userDto.RegisteredAt, createdUser.RegisteredAt);

            await _userService.DeleteEntityAsync(createdUser.Id);
        }


        [Fact]
        public async Task AddUser_WhenUserDtoIsNull_ThenThrowArgumentNullException()
        {
            UserDTO userDto = null;

            await Assert.ThrowsAsync<ArgumentNullException>(async () => 
                    await _userService.AddEntityAsync(userDto));
        }


        [Fact]
        public async Task UpdateUser_WhenAddUserToTeam_ThenUserTeamIdNotEqualNull()
        {
            var user = await _userService.GetEntityAsync(1);

            user.TeamId = 1;

            var updatedUser = await _userService.UpdateEntityAsync(user);

            Assert.Equal(1, updatedUser.TeamId);
        }
    }
}
