﻿using System;
using AutoMapper;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Models = ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.Tests.Abstract
{
    public abstract class BaseTestClass
    {
        protected readonly ApplicationContext _context;
        protected readonly IMapper _mapper;
        protected readonly DbContextOptions<ApplicationContext> _options;

        public BaseTestClass()
        {
            _options = new DbContextOptionsBuilder<ApplicationContext>()
                    .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()).Options;

            _context = new ApplicationContext(_options);

            Seed();

            var config = new MapperConfiguration(opts =>
            {
                opts.CreateMap<Models.User, UserDTO>().ReverseMap();
                opts.CreateMap<Models.Team, TeamDTO>().ReverseMap();
                opts.CreateMap<Models.Task, TaskDTO>().ReverseMap();
                opts.CreateMap<Models.Project, ProjectDTO>().ReverseMap();
            });

            _mapper = config.CreateMapper();
        }

        protected void Dispose()
        {
            _context.Dispose();
        }


        private void Seed()
        {
            using (var context = new ApplicationContext(_options))
            {
                var teams = new List<Models.Team>()
                {
                    new Models.Team
                    {
                        Name = "Team 1",
                        CreatedAt = DateTime.Now
                    },
                    new Models.Team
                    {
                        Name = "Team 2",
                        CreatedAt = DateTime.Now
                    },
                    new Models.Team
                    {
                        Name = "Team 3",
                        CreatedAt = DateTime.Now
                    },
                    new Models.Team
                    {
                        Name = "Team 4",
                        CreatedAt = DateTime.Now
                    }
                };


                var user = new Models.User
                {
                    Id = 1,
                    Email = "user@server.com",
                    FirstName = "Peter",
                    LastName = "Pen",
                    BirthDay = DateTime.Now,
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                };


                var project = new Models.Project
                {
                    AuthorId = 1,
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Now,
                    Description = "Project description",
                    Name = "Project Name",
                    TeamId = 1
                };

                var tasks = new List<Models.Task>()
                {
                    new Models.Task
                    {
                        CreatedAt = DateTime.Parse("2020-01-01T00:00:00.00"),
                        FinishedAt = null,
                        Description = "Some description",
                        Name = "Very hard task",
                        PerformerId = 1,
                        ProjectId = 1,
                        State = DAL.Enums.TaskState.ToDo
                    },
                    new Models.Task
                    {
                        CreatedAt = DateTime.Parse("2020-01-01T00:00:00.00"),
                        FinishedAt = DateTime.Parse("2021-01-01T00:00:00.00"),
                        Description = "Some description",
                        Name = "Very hard task",
                        PerformerId = 2,
                        ProjectId = 1,
                        State = DAL.Enums.TaskState.Done
                    }
                };



                context.Teams.AddRange(teams);
                context.Users.Add(user);
                context.Projects.Add(project);
                context.Tasks.AddRange(tasks);

                context.SaveChanges();
            }
        }
    }
}
