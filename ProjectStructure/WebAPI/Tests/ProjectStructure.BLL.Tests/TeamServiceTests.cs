﻿using Xunit;
using System;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Abstract;

namespace ProjectStructure.BLL.Tests
{
    public class TeamServiceTests : BaseTestClass
    {
        private readonly TeamService _teamService;

        public TeamServiceTests()
        {
            _teamService = new TeamService(_context, _mapper);
        }


        [Fact]
        public async Task GetTeams_WhenGetAllTeam_ThenAmountFour()
        {
            var teams = await _teamService.GetAllEntitiesAsync();

            Assert.Equal(4, teams.Count());
        }


        [Fact]
        public async Task AddTeam_WhenAddTeam_ThenAmountTwoAndCorrespondedBody()
        {
            var teamDto = new TeamDTO
            {
                Name = "Second team",
                CreatedAt = DateTime.Now
            };

            var createdTeam = await _teamService.AddEntityAsync(teamDto);

            var teams = await _teamService.GetAllEntitiesAsync();

            Assert.Equal(5, teams.Count());
            Assert.Equal(teamDto.CreatedAt, createdTeam.CreatedAt);
            Assert.Equal(teamDto.Name, createdTeam.Name);

            await _teamService.DeleteEntityAsync(createdTeam.Id);
        }


        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        public async Task DeleteTeam_WhenDeleteTeam_ThenThereIsNoSuchAnEntity(int id)
        {
            var deletedId = await _teamService.DeleteEntityAsync(id);

            var deletedTeam = await _teamService.GetEntityAsync(deletedId);

            Assert.Null(deletedTeam);
        }
    }
}
