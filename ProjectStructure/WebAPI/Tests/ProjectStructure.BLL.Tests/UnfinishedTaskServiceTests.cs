﻿using Xunit;
using System;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Abstract;

namespace ProjectStructure.BLL.Tests
{
    public class UnfinishedTaskServiceTests : BaseTestClass
    {
        private readonly UnfinishedTaskService _unfinishedTaskService;

        public UnfinishedTaskServiceTests()
        {
            _unfinishedTaskService = new UnfinishedTaskService(_context, _mapper);
        }


        [Fact]
        public async Task GetUnfinishedTasks_WhenThereAreNotUnfinishedTasks_ThenListIsEmpty()
        {
            // Arrange
            var tasksService = new TaskService(_context, _mapper);
            var userService = new UserService(_context, _mapper);

            // Add a new 'clear' user
            var newUserDto = new UserDTO
            {
                BirthDay = DateTime.Now,
                Email = "user@server.com",
                FirstName = "Donald",
                LastName = "Tramp",
                RegisteredAt = DateTime.Now,
                TeamId = 1
            };

            var createdUser = await userService.AddEntityAsync(newUserDto);

            // Add new finished tasts
            var tasks = new List<TaskDTO>()
            {
                new TaskDTO
                {
                    FinishedAt = DateTime.Now,
                    CreatedAt = DateTime.Now,
                    Description = "Test finished task",
                    Name = "Finished task",
                    PerformerId = createdUser.Id,
                    ProjectId = 1,
                    State = DAL.Enums.TaskState.Done
                },
                new TaskDTO
                {
                    FinishedAt = DateTime.Now,
                    CreatedAt = DateTime.Now,
                    Description = "Test finished task",
                    Name = "Finished task",
                    PerformerId = createdUser.Id,
                    ProjectId = 1,
                    State = DAL.Enums.TaskState.Done
                }
            };

            var task1 = await tasksService.AddEntityAsync(tasks[0]);
            var task2 = await tasksService.AddEntityAsync(tasks[1]);


            // Act
            var unfinishedTasks = await _unfinishedTaskService.GetUnfinishedTasksAsync(createdUser.Id);

            Assert.Empty(unfinishedTasks);

            await tasksService.DeleteEntityAsync(task1.Id);
            await tasksService.DeleteEntityAsync(task2.Id);
            await userService.DeleteEntityAsync(createdUser.Id);
        }



        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(15)]
        [InlineData(20)]
        public async Task GetUnfinishedTasks_WhenGotTasks_ThenAllTheTasksAreUnfinished(int performerId)
        {
            var tasks = await _unfinishedTaskService.GetUnfinishedTasksAsync(performerId);

            if (tasks.Count != 0)
                Assert.All(tasks, task => Assert.True(task.FinishedAt == null));
            else
                Assert.True(true);
        }



        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        [InlineData(7)]
        [InlineData(9)]
        [InlineData(11)]
        public async Task GetUnfinishedTasks_WhenAddUnfinishedTaskToUser_ThenUserHasAtLeastOneUnfinishedTask(int performerId)
        {
            var tasksService = new TaskService(_context, _mapper);

            var task = new TaskDTO
            {
                FinishedAt = null,
                CreatedAt = DateTime.Now,
                Description = "Test unfinished task",
                Name = "Unfinished task",
                PerformerId = performerId,
                ProjectId = 1,
                State = DAL.Enums.TaskState.ToDo
            };

            var createdTask = await tasksService.AddEntityAsync(task);

            var tasks = await _unfinishedTaskService.GetUnfinishedTasksAsync(performerId);

            Assert.NotNull(tasks);
            Assert.NotEmpty(tasks);
            Assert.All(tasks, task => Assert.True(task.FinishedAt == null));

            await tasksService.DeleteEntityAsync(createdTask.Id);
        }



        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task GetUnfinishedTasks_WhenPerformerIdIsOutOfRange_ThenArgumentOutOfRangeException(int performerId)
        {
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => 
                await _unfinishedTaskService.GetUnfinishedTasksAsync(performerId));
        }
    }
}
