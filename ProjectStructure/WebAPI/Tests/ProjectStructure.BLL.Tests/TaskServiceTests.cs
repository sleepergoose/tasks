﻿using Xunit;
using System;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Abstract;

namespace ProjectStructure.BLL.Tests
{
    public class TaskServiceTests : BaseTestClass
    {
        private readonly TaskService _taskService;

        public TaskServiceTests()
        {
            _taskService = new TaskService(_context, _mapper);
        }


        [Fact]
        public async Task AddTask_WhenTaskAdded_ThenThreeTasksInStorage()
        {
            var taskDto = new TaskDTO
            {
                CreatedAt = DateTime.Parse("2020-01-01T00:00:00.00"),
                FinishedAt = null,
                Description = "Some description",
                Name = "Added second task",
                PerformerId = 1,
                ProjectId = 1,
                State = DAL.Enums.TaskState.ToDo
            };

            var createdTask = await _taskService.AddEntityAsync(taskDto);

            var tasks = await _taskService.GetAllEntitiesAsync();

            Assert.Equal(3, tasks.Count());
            Assert.Equal(taskDto.Name, createdTask.Name);

            await _taskService.DeleteEntityAsync(createdTask.Id);
        }



        [Fact]
        public async Task UpdateTask_WhenTaskFinished_ThenTaskStateIsDone()
        {
            var updatedTask = new TaskDTO
            {
                Id = 1,
                FinishedAt = DateTime.Now,
                State = DAL.Enums.TaskState.Done
            };

            var result = await _taskService.UpdateEntityAsync(updatedTask);

            Assert.Equal(updatedTask.State, result.State);
            Assert.Equal(updatedTask.FinishedAt, result.FinishedAt);
        }


        [Fact]
        public async Task UpdateTask_WhenThereIsNoSuchId_ThenException()
        {
            var updatedTask = new TaskDTO
            {
                Id = 3,
                FinishedAt = DateTime.Now,
                State = DAL.Enums.TaskState.Done
            };

            await Assert.ThrowsAsync<Exception>(async () => await _taskService.UpdateEntityAsync(updatedTask));
        }


        [Fact]
        public async Task UpdateTask_WhenTaskDtoIsNull_ThenThrowArgumentNullException()
        {
            TaskDTO taskDto = null;

            await Assert.ThrowsAsync<ArgumentNullException>(async () => 
                                                await _taskService.UpdateEntityAsync(taskDto));
        }
    }
}
