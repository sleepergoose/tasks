﻿using Xunit;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Abstract;

namespace ProjectStructure.BLL.Tests
{
    public class LinqServiceTests : BaseTestClass
    {
        private readonly LinqService _linqService;

        public LinqServiceTests()
        {
            _linqService = new LinqService(_context, _mapper);
        }


        [Fact]
        public async Task GetTasksList_WhenPerformerIdEquals1_ThenOneTasks()
        {
            int performerId = 1;

            var tasks = await _linqService.GetTasksListAsync(performerId);

            Assert.Single(tasks);
        }


        [Fact]
        public async Task GetTasksList_WhenPerformerIdEquals3_ThenThereIsNoTasks()
        {
            int performerId = 3;

            var tasks = await _linqService.GetTasksListAsync(performerId);

            Assert.Empty(tasks);
        }


        [Fact]
        public async Task GetTasksAmount_WhenAuthorIdIs1_ThenProjectTasksAmountEquals2()
        {
            int authorId = 1;

            var dict = await _linqService.GetTasksAmountAsync(authorId);

            Assert.Equal(1, dict.FirstOrDefault().Key.Id);
            Assert.Equal(2, dict.FirstOrDefault().Value);
            Assert.Equal(1, dict.FirstOrDefault().Key.AuthorId);
        }


        [Fact]
        public async Task GetTasksAmount_WhenAuthorIdIs2_ThenTasksIsEmpty()
        {
            int authorId = 2;

            var dict = await _linqService.GetTasksAmountAsync(authorId);

            Assert.Empty(dict);
        }


        [Fact]
        public async Task GetFinishedTasks_WhenPerfomerIdEquals1_ThenFinishedTasksIsEmpty()
        {
            int perfomerId = 1;

            var tasks = await _linqService.GetFinishedTasksAsync(perfomerId);

            Assert.Empty(tasks);
        }

        [Fact]
        public async Task GetFinishedTasks_WhenPerfomerIdEquals2_ThenFinishedTasksIsSingle()
        {
            int perfomerId = 2;

            var tasks = await _linqService.GetFinishedTasksAsync(perfomerId);

            Assert.Single(tasks);
        }
    }
}
