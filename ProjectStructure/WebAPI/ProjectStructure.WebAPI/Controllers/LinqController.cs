﻿using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;


        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }


        /* -- 1 -- */
        [HttpGet("TasksAmount/{id}")]
        public async Task<ActionResult<Dictionary<ProjectDTO, int>>> GetTasksAmount(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var dict = (await _linqService.GetTasksAmountAsync(id)).ToList();

            if (dict == null)
                return NoContent();

            return Ok(dict);
        }


        /* -- 2 -- */
        [HttpGet("TasksList/{id}")]
        public async Task<ActionResult<List<TaskDTO>>> GetTasksList(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var tasks = await _linqService.GetTasksListAsync(id);

            if (tasks == null)
                return NoContent();

            return Ok(tasks);
        }


        /* -- 3 -- */
        [HttpGet("FinishedTasks/{id}")]
        public async Task<ActionResult<List<(int Id, string Name)>>> GetFinishedTasks(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var tasks = await _linqService.GetFinishedTasksAsync(id);

            if (tasks == null)
                return NoContent();

            return Ok(JsonConvert.SerializeObject(tasks));
        }


        /* -- 4 -- */
        [HttpGet("TeamsMembers")]
        public async Task<ActionResult<List<(int Id, string TeamName, List<UserDTO> Users)>>> GetTeamsMembers()
        {
            var teams = await _linqService.GetTeamsMembersAsync();

            if (teams == null)
                return NoContent();

            return Ok(JsonConvert.SerializeObject(teams));
        }


        /* -- 5 -- */
        [HttpGet("UsersWithTasks")]
        public async Task<ActionResult<List<UserWithTasksDTO>>> GetUsersWithTasks()
        {
            var users = await _linqService.GetUsersWithTasksAsync();

            if (users == null)
                return NoContent();

            return Ok(users);
        }


        /* -- 6 -- */
        [HttpGet("UserSummary/{id}")]
        public async Task<ActionResult<UserSummaryDTO>> GetUserSummary(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var user = await _linqService.GetUserSummaryAsync(id);

            if (user == null)
                return NoContent();

            return Ok(user);
        }


        /* -- 7 -- */
        [HttpGet("ProjectSummary")]
        public async Task<ActionResult<List<ProjectSummaryDTO>>> GetProjectSummary()
        {
            var projects = await _linqService.GetProjectSummaryAsync();

            if (projects == null)
                return NoContent();

            return Ok(projects);
        }
    }
}
