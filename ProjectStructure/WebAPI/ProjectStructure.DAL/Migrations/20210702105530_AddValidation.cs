﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddValidation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RegisteredAt",
                table: "Users",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "GETDATE()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                type: "varchar(64)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                type: "varchar(64)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "varchar(64)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "BirthDay",
                table: "Users",
                type: "datetime2(2)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "Teams",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "GETDATE()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<string>(
                name: "TaskName",
                table: "Tasks",
                type: "nvarchar(128)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FinishedAt",
                table: "Tasks",
                type: "datetime2(2)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                type: "nvarchar(512)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "Tasks",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "GETDATE()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<string>(
                name: "ProjectName",
                table: "Projects",
                type: "nvarchar(128)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                type: "nvarchar(512)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Deadline",
                table: "Projects",
                type: "datetime2(2)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "Projects",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "GETDATE()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 13, new DateTime(2018, 11, 21, 0, 46, 30, 262, DateTimeKind.Unspecified).AddTicks(9999), new DateTime(2021, 11, 12, 18, 54, 38, 171, DateTimeKind.Local).AddTicks(5223), "Asperiores possimus consequuntur molestiae velit explicabo culpa velit eligendi et consectetur nam et dolores eius.", "Aut eos vel sed possimus dolores.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 22, new DateTime(2019, 4, 3, 2, 0, 16, 876, DateTimeKind.Unspecified).AddTicks(3686), new DateTime(2022, 11, 27, 9, 37, 45, 937, DateTimeKind.Local).AddTicks(3599), "Commodi quia omnis expedita qui dolor odit pariatur aut sed eos cupiditate voluptatem quia recusandae.", "Molestiae officiis ipsam aut adipisci deserunt.", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 20, new DateTime(2020, 4, 7, 3, 55, 46, 941, DateTimeKind.Unspecified).AddTicks(962), new DateTime(2021, 8, 18, 13, 30, 56, 357, DateTimeKind.Local).AddTicks(5025), "Libero eum et sit dolorum soluta est recusandae suscipit fugiat et illum ut unde quia.", "Vero et et placeat nihil error.", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 28, new DateTime(2019, 5, 11, 22, 51, 44, 578, DateTimeKind.Unspecified).AddTicks(1564), new DateTime(2021, 10, 27, 15, 8, 48, 497, DateTimeKind.Local).AddTicks(7593), "Nemo deserunt quidem suscipit sapiente quo nihil illo explicabo voluptatum in eos occaecati aliquid vero.", "Ea quisquam quia tempora accusantium quos.", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 14, new DateTime(2020, 1, 18, 2, 30, 7, 685, DateTimeKind.Unspecified).AddTicks(6462), new DateTime(2022, 3, 31, 9, 17, 40, 956, DateTimeKind.Local).AddTicks(2201), "Vel consequatur voluptatem ut nulla praesentium nesciunt eius quo ipsam dicta perferendis mollitia quis sed.", "Perferendis sed voluptatem ipsum aliquid est.", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 21, new DateTime(2019, 7, 27, 8, 31, 19, 535, DateTimeKind.Unspecified).AddTicks(7996), new DateTime(2021, 11, 12, 11, 48, 52, 802, DateTimeKind.Local).AddTicks(7247), "Magni minima at aut quis quis nihil veniam error voluptatem impedit ad impedit magnam perferendis.", "Quisquam modi exercitationem exercitationem id ratione.", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 2, new DateTime(2019, 12, 2, 23, 17, 22, 519, DateTimeKind.Unspecified).AddTicks(5637), new DateTime(2022, 6, 29, 9, 15, 47, 244, DateTimeKind.Local).AddTicks(9979), "Ea in explicabo harum unde sint quo velit sed quo dolor corporis non laudantium accusantium.", "Est sed doloribus iste aliquam sint.", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 16, new DateTime(2020, 6, 13, 5, 50, 14, 42, DateTimeKind.Unspecified).AddTicks(4960), new DateTime(2022, 12, 28, 4, 42, 50, 11, DateTimeKind.Local).AddTicks(9612), "Qui laborum quaerat delectus earum eos est animi quos perferendis impedit voluptas et perferendis quidem.", "Velit modi illo adipisci inventore deleniti." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 22, new DateTime(2019, 10, 25, 7, 25, 53, 852, DateTimeKind.Unspecified).AddTicks(2025), new DateTime(2022, 3, 8, 14, 58, 13, 432, DateTimeKind.Local).AddTicks(3679), "Rem tenetur suscipit voluptas exercitationem nemo eum aperiam voluptatem delectus et blanditiis ducimus quaerat sunt.", "Quo earum et numquam aut fugit." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 16, new DateTime(2018, 3, 4, 14, 59, 6, 820, DateTimeKind.Unspecified).AddTicks(3498), new DateTime(2021, 12, 27, 20, 7, 22, 563, DateTimeKind.Local).AddTicks(6709), "Ea unde expedita modi ut voluptatem qui dolore pariatur rem sit iure possimus velit impedit.", "Voluptatem sit beatae voluptatem qui quam.", 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 13, 15, 41, 42, 805, DateTimeKind.Unspecified).AddTicks(4813), "Porro voluptas est dolor.", new DateTime(2020, 8, 27, 23, 46, 20, 9, DateTimeKind.Unspecified).AddTicks(3909), "Minus sed esse dolore sapiente.", 23, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 14, 14, 57, 40, 639, DateTimeKind.Unspecified).AddTicks(2441), "mollitia", new DateTime(2020, 8, 8, 4, 45, 24, 545, DateTimeKind.Unspecified).AddTicks(4750), "Est vel laborum vel molestias.", 3, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 10, 22, 9, 39, 662, DateTimeKind.Unspecified).AddTicks(9428), "Eligendi quo corporis et harum. Quasi beatae dolore harum sit ut hic. Deleniti rerum ut modi voluptas illum praesentium. Ipsam nostrum velit voluptatem optio quod illum. Eveniet perspiciatis maiores praesentium doloremque aut enim eum debitis est. Impedit eos iusto laborum magnam autem reprehenderit.", new DateTime(2018, 6, 13, 1, 29, 17, 102, DateTimeKind.Unspecified).AddTicks(8208), "Asperiores eaque in ut at.", 23, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 23, 22, 29, 36, 491, DateTimeKind.Unspecified).AddTicks(5352), "Voluptatem veniam minus temporibus minima.\nSunt quas in at ut soluta.\nPlaceat iusto debitis cum hic.\nCorporis dolor consequuntur tempora.\nDoloribus eos et voluptates qui.\nVoluptatem ut repellendus velit ducimus.", "Voluptas sed cupiditate aut incidunt.", 25, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 9, 24, 0, 50, 48, 112, DateTimeKind.Unspecified).AddTicks(4162), "Nobis facilis suscipit. Vel natus ratione dolores inventore. Et esse officiis nam ab minima. Beatae saepe explicabo corporis in dolor pariatur aut. Atque quia perferendis nisi maiores fugit accusantium cupiditate modi aut. Et necessitatibus modi ab ipsam quibusdam ut accusantium iste molestiae.", null, "Qui inventore fugit rerum veniam.", 26, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 5, 27, 16, 43, 23, 860, DateTimeKind.Unspecified).AddTicks(412), "Qui beatae accusantium quia porro.", null, "Reiciendis quod temporibus odit totam.", 12, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 4, 22, 42, 25, 23, DateTimeKind.Unspecified).AddTicks(9190), "doloribus", new DateTime(2019, 2, 22, 0, 24, 8, 304, DateTimeKind.Unspecified).AddTicks(4661), "Dolore ipsam quo consectetur tempore.", 25, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 8, 28, 13, 1, 42, 803, DateTimeKind.Unspecified).AddTicks(8362), "Fugit laboriosam voluptatum natus iusto qui voluptatem consectetur.", null, "Fugit quia nesciunt omnis dolores.", 11, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 13, 9, 36, 52, 309, DateTimeKind.Unspecified).AddTicks(6821), "Et voluptas occaecati distinctio necessitatibus tenetur facere repudiandae.", null, "Porro omnis occaecati ut sit.", 22, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 9, 15, 5, 56, 58, 583, DateTimeKind.Unspecified).AddTicks(5729), "Quibusdam optio adipisci. Aliquam nihil est aliquid expedita vel. Eum deserunt recusandae repellendus illum illum libero atque reprehenderit. Dolore non quae. Inventore praesentium iure at nobis quasi ut omnis libero.", new DateTime(2019, 11, 6, 12, 57, 31, 697, DateTimeKind.Unspecified).AddTicks(6554), "Et voluptas et quo minima.", 10, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 28, 5, 45, 4, 831, DateTimeKind.Unspecified).AddTicks(7812), "Ut sunt sit est enim eos repudiandae omnis.\nRatione eligendi velit delectus.\nDolores ut maxime quos.\nEt molestiae nesciunt.", "Qui natus nostrum quod rerum.", 2, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 8, 18, 10, 46, 30, 402, DateTimeKind.Unspecified).AddTicks(6123), "Voluptas nam praesentium asperiores quidem facere blanditiis blanditiis sit.\nIusto error et et voluptatem repellat.\nNam deserunt possimus eum animi vel.\nIpsam saepe consequatur mollitia iure quae et.\nAmet quis qui nemo veniam dolores quam dolorem.", new DateTime(2019, 5, 17, 13, 34, 55, 360, DateTimeKind.Unspecified).AddTicks(6668), "Unde tenetur repellendus aspernatur architecto.", 10, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 4, 20, 27, 50, 201, DateTimeKind.Unspecified).AddTicks(8917), "Voluptatem totam dolorem quis necessitatibus soluta maxime iure vero quasi.", new DateTime(2019, 3, 5, 5, 43, 37, 348, DateTimeKind.Unspecified).AddTicks(9765), "Voluptas deleniti quidem temporibus facilis.", 24, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 8, 31, 12, 57, 0, 342, DateTimeKind.Unspecified).AddTicks(7249), "Nostrum modi aut et error necessitatibus a mollitia.", new DateTime(2019, 12, 24, 9, 39, 40, 261, DateTimeKind.Unspecified).AddTicks(4407), "Aut quo a et nihil.", 22, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 16, 23, 28, 12, 711, DateTimeKind.Unspecified).AddTicks(4508), "Nostrum illum perferendis omnis.\nEt beatae est dolorem.\nId repellendus officiis et sint possimus dolor.\nUt voluptatum minus distinctio beatae non.\nAssumenda doloribus voluptatem eos similique rerum libero illo.", new DateTime(2020, 8, 7, 4, 6, 31, 200, DateTimeKind.Unspecified).AddTicks(8077), "Voluptatem qui illum cum dolor.", 5, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 10, 22, 58, 8, 236, DateTimeKind.Unspecified).AddTicks(6670), "Eligendi a voluptatum.", "Sit occaecati impedit ipsa dolor.", 9, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 9, 20, 39, 20, 29, DateTimeKind.Unspecified).AddTicks(3312), "Possimus omnis mollitia rerum quasi qui facere dolores.", new DateTime(2021, 5, 6, 4, 45, 39, 50, DateTimeKind.Unspecified).AddTicks(8936), "Odio et quod aut dolores.", 12, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 9, 0, 28, 20, 351, DateTimeKind.Unspecified).AddTicks(3493), "aperiam", new DateTime(2021, 2, 26, 6, 24, 56, 17, DateTimeKind.Unspecified).AddTicks(9149), "Voluptatem dolorum ut dolor occaecati.", 3, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 11, 20, 2, 31, 162, DateTimeKind.Unspecified).AddTicks(3331), "Omnis nesciunt dignissimos exercitationem ex reprehenderit itaque qui.", "Soluta corporis esse aut et.", 4, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 1, 23, 10, 27, 12, 257, DateTimeKind.Unspecified).AddTicks(9380), "Repudiandae rem cupiditate cupiditate sed eum architecto.\nNisi facere id iure officiis consectetur et quia est.\nConsequuntur qui non consequuntur officiis delectus.", null, "Velit esse aut quo voluptatem.", 22, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 4, 10, 48, 39, 175, DateTimeKind.Unspecified).AddTicks(6817), "Assumenda labore error eos doloribus quis blanditiis quis eos enim. Omnis fugiat incidunt. Corrupti sequi qui temporibus mollitia iste optio. Consequuntur consequatur ut voluptas tempore. Voluptatem repellendus ut dolor aliquam quo amet natus.", new DateTime(2020, 6, 2, 13, 32, 15, 67, DateTimeKind.Unspecified).AddTicks(5479), "In beatae eaque quasi sunt.", 23, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 7, 0, 48, 616, DateTimeKind.Unspecified).AddTicks(9545), "Repudiandae eligendi inventore sunt commodi architecto non.", new DateTime(2020, 5, 11, 13, 15, 41, 195, DateTimeKind.Unspecified).AddTicks(253), "Sed optio quo praesentium ut.", 14, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 5, 19, 18, 14, 34, 173, DateTimeKind.Unspecified).AddTicks(2009), "Molestiae officiis dolor eaque dolor sapiente.", "Qui atque distinctio autem in.", 13, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 7, 30, 1, 41, 3, 211, DateTimeKind.Unspecified).AddTicks(2919), "Saepe voluptas aliquam.", new DateTime(2021, 3, 24, 17, 27, 40, 87, DateTimeKind.Unspecified).AddTicks(7891), "Quam adipisci qui ea sunt.", 23, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 16, 0, 19, 32, 185, DateTimeKind.Unspecified).AddTicks(8373), "Ipsa sint sit voluptates in quasi nesciunt perferendis. Quis consectetur officia non esse reprehenderit quis quis soluta fuga. Aut facilis velit saepe.", "Accusamus illo eaque laboriosam rerum.", 4, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 27, 18, 45, 39, 718, DateTimeKind.Unspecified).AddTicks(5849), "Enim dolor quos optio explicabo odit enim.", new DateTime(2020, 4, 26, 0, 3, 25, 851, DateTimeKind.Unspecified).AddTicks(5328), "Facere adipisci laudantium consequuntur sint.", 2, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 11, 18, 4, 52, 18, 317, DateTimeKind.Unspecified).AddTicks(1908), "Quia consequatur ea. Odio fuga sed quia est praesentium perferendis illo illum aspernatur. Consectetur non et. Quis perferendis earum quos est cum inventore est.", new DateTime(2021, 5, 18, 12, 33, 53, 127, DateTimeKind.Unspecified).AddTicks(9620), "Nam illo sed voluptas voluptates.", 5, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 26, 6, 16, 31, 393, DateTimeKind.Unspecified).AddTicks(6273), "Eius incidunt deserunt.\nIn nulla similique rerum perferendis in.\nQuia accusantium sunt sit debitis quo quaerat eum ut.\nSit officia labore ipsam sunt ut tenetur.", "Rem ratione consequatur magni omnis.", 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 10, 6, 7, 6, 281, DateTimeKind.Unspecified).AddTicks(214), "Nesciunt facere voluptatibus id.", new DateTime(2021, 4, 9, 16, 11, 37, 152, DateTimeKind.Unspecified).AddTicks(7018), "Corporis quibusdam impedit eveniet ut.", 29, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 1, 6, 29, 58, 76, DateTimeKind.Unspecified).AddTicks(758), "rerum", new DateTime(2020, 4, 10, 2, 27, 31, 630, DateTimeKind.Unspecified).AddTicks(5831), "Quam quas ut culpa error.", 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 7, 29, 5, 294, DateTimeKind.Unspecified).AddTicks(4151), "Reiciendis dignissimos voluptatibus quia maxime placeat.", null, "A incidunt et et quisquam.", 26, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 8, 3, 56, 31, 704, DateTimeKind.Unspecified).AddTicks(6434), "quidem", "Delectus doloribus commodi rerum nam.", 3, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 1, 23, 43, 38, 201, DateTimeKind.Unspecified).AddTicks(9855), "Eius dicta quos a aut occaecati explicabo quo.\nQuo temporibus quos quia neque provident fuga quis voluptatem.\nQuis ut voluptatibus sed ea et labore.\nLaudantium sed facilis.\nNisi repudiandae quasi non.\nDolores voluptate nam ratione ab quibusdam et sint minima illo.", new DateTime(2020, 6, 23, 17, 31, 35, 528, DateTimeKind.Unspecified).AddTicks(1638), "Delectus impedit perspiciatis odit accusantium.", 6, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 19, 3, 24, 30, 776, DateTimeKind.Unspecified).AddTicks(2164), "cupiditate", null, "Earum distinctio ea labore unde.", 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 17, 1, 17, 16, 916, DateTimeKind.Unspecified).AddTicks(9555), "Et quam ut totam veritatis dolorum quia vel ex eius.\nVeniam dolorem totam ea aliquam.", new DateTime(2021, 4, 7, 14, 52, 20, 645, DateTimeKind.Unspecified).AddTicks(4737), "Nam dignissimos in magnam ut.", 10, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 3, 23, 1, 31, 0, 662, DateTimeKind.Unspecified).AddTicks(7594), "Error cupiditate repellendus itaque illo occaecati molestiae recusandae. Non delectus qui autem. Nisi aliquid ut velit ipsa. Qui perspiciatis quia est repellat.", null, "Rerum nihil neque maxime aliquam.", 25, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 12, 23, 23, 6, 25, 424, DateTimeKind.Unspecified).AddTicks(2340), "Velit aut eum minus sint ea odio quod iste ex. Consequatur in inventore. Sequi ab voluptatem velit veritatis earum suscipit maxime et. Eos error soluta labore et harum in at.", new DateTime(2018, 2, 7, 14, 11, 55, 889, DateTimeKind.Unspecified).AddTicks(9487), "Consequatur quis ut est et.", 5, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 9, 26, 14, 26, 48, 663, DateTimeKind.Unspecified).AddTicks(9334), "Nemo dolores maxime cum et error iusto rerum architecto eos.\nAut expedita voluptas omnis.\nUt quisquam quas a itaque enim harum.\nAdipisci doloribus minima omnis est quia quia rerum.", new DateTime(2021, 6, 5, 1, 37, 26, 794, DateTimeKind.Unspecified).AddTicks(395), "Sint quis aliquam neque quam.", 8, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 8, 17, 20, 1, 242, DateTimeKind.Unspecified).AddTicks(7322), "veniam", new DateTime(2018, 11, 29, 18, 11, 0, 564, DateTimeKind.Unspecified).AddTicks(5423), "Alias perferendis quam sint quo.", 3, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 1, 9, 28, 22, 839, DateTimeKind.Unspecified).AddTicks(5826), "Quidem veniam expedita quaerat ut illum. Delectus quo qui in consequatur. Porro corrupti sed nihil natus est vel ducimus et sed.", new DateTime(2020, 2, 3, 20, 53, 31, 721, DateTimeKind.Unspecified).AddTicks(681), "Optio tempora sed reiciendis eaque.", 28, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 3, 6, 50, 50, 60, DateTimeKind.Unspecified).AddTicks(5951), "Voluptas aliquam vel quia tenetur dolor distinctio dolores.", new DateTime(2018, 1, 5, 6, 35, 4, 535, DateTimeKind.Unspecified).AddTicks(153), "Sapiente numquam voluptatem voluptate et.", 11, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 10, 7, 19, 56, 35, 220, DateTimeKind.Unspecified).AddTicks(7512), "vitae", new DateTime(2019, 4, 18, 10, 33, 58, 150, DateTimeKind.Unspecified).AddTicks(1872), "Iste deleniti aliquam nihil doloremque.", 30, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 30, 13, 15, 6, 672, DateTimeKind.Unspecified).AddTicks(2585), "Assumenda dolorem placeat ea nihil nam tenetur nihil ullam.", new DateTime(2019, 6, 4, 9, 6, 7, 897, DateTimeKind.Unspecified).AddTicks(6256), "Placeat consequatur aliquam mollitia nihil.", 20, 6, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 6, 7, 9, 59, 48, 170, DateTimeKind.Unspecified).AddTicks(9788), "Dolorem perferendis veniam velit repudiandae consequuntur optio voluptatem repudiandae et.\nUt reprehenderit qui at minima consequatur et autem aut qui.\nUnde possimus omnis nisi harum consectetur.", null, "Id et nihil totam nisi.", 1, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 1, 24, 7, 51, 12, 926, DateTimeKind.Unspecified).AddTicks(3616), "culpa", new DateTime(2019, 4, 6, 18, 44, 24, 592, DateTimeKind.Unspecified).AddTicks(7005), "Ut et iure qui magnam.", 20, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 10, 5, 31, 51, 443, DateTimeKind.Unspecified).AddTicks(3262), "Aliquam temporibus quam totam non.", null, "Perferendis consequatur numquam nihil aut.", 15, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 3, 24, 14, 22, 15, 709, DateTimeKind.Unspecified).AddTicks(5900), "Fugit voluptatem autem et. Quod provident dolorem. Voluptas fuga reiciendis rem at qui. Sequi ducimus quas sint et laborum rem dicta aspernatur.", new DateTime(2021, 1, 7, 3, 24, 29, 665, DateTimeKind.Unspecified).AddTicks(6399), "A dolorum debitis dolorem sequi.", 12, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 8, 12, 23, 51, 332, DateTimeKind.Unspecified).AddTicks(5106), "Soluta cum recusandae nihil modi et accusamus non repellendus.", new DateTime(2019, 1, 26, 21, 42, 51, 114, DateTimeKind.Unspecified).AddTicks(1243), "Velit maxime veniam maiores aliquam.", 2, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 16, 3, 34, 32, 496, DateTimeKind.Unspecified).AddTicks(1877), "Aperiam dolores assumenda debitis laudantium.", new DateTime(2019, 3, 28, 23, 56, 2, 705, DateTimeKind.Unspecified).AddTicks(2107), "Unde cupiditate et omnis nihil.", 6, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 8, 15, 11, 49, 37, 598, DateTimeKind.Unspecified).AddTicks(8494), "Eius similique asperiores animi.", new DateTime(2018, 9, 12, 10, 49, 38, 165, DateTimeKind.Unspecified).AddTicks(8001), "Qui voluptas eum voluptatibus repudiandae.", 26, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 9, 0, 9, 2, 760, DateTimeKind.Unspecified).AddTicks(1221), "Et recusandae nisi omnis dignissimos ullam animi et.", new DateTime(2019, 12, 24, 9, 57, 56, 578, DateTimeKind.Unspecified).AddTicks(6839), "Omnis corrupti nihil est ea.", 9, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 26, 16, 48, 40, 644, DateTimeKind.Unspecified).AddTicks(403), "Et facere perferendis enim sed doloremque.", "Laborum aut quia corrupti eum.", 24, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 2, 26, 8, 22, 3, 667, DateTimeKind.Unspecified).AddTicks(2393), "Enim reprehenderit ea sunt voluptas officia non esse harum.", new DateTime(2018, 1, 23, 13, 9, 38, 21, DateTimeKind.Unspecified).AddTicks(2373), "Velit amet sit reiciendis quae.", 12, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 12, 30, 15, 12, 14, 441, DateTimeKind.Unspecified).AddTicks(1837), "Et facilis aut ut. Nostrum eum quidem veritatis accusamus impedit natus. Architecto sed molestiae nihil aspernatur quia qui minima alias iure. Excepturi reprehenderit reiciendis aut autem.", null, "Non ipsum sit sit totam.", 27, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 27, 21, 47, 26, 999, DateTimeKind.Unspecified).AddTicks(2794), "magnam", new DateTime(2018, 4, 5, 14, 7, 31, 537, DateTimeKind.Unspecified).AddTicks(1901), "Dolores ducimus eius aliquid soluta.", 16, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 9, 17, 3, 32, 2, 821, DateTimeKind.Unspecified).AddTicks(2401), "Et aliquam illum est eos ipsam quis voluptas nisi rerum. Natus est in dicta non quasi esse repellat ratione. Est officiis impedit perspiciatis voluptatem voluptas autem eaque vel. Dicta consequuntur quia harum consectetur in et. Aut aut libero ut et quod ea. Natus unde unde fugiat tenetur.", null, "Dolores quidem possimus et quis.", 4, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 17, 15, 6, 7, 406, DateTimeKind.Unspecified).AddTicks(5620), "Facere iusto in rerum saepe. Dignissimos debitis adipisci ut aspernatur porro dolore ut eos. Veniam dolorem corrupti reiciendis distinctio ullam qui adipisci qui vel. Dignissimos quisquam sit aut eos. Ea doloribus ut hic laborum et.", new DateTime(2018, 2, 7, 10, 43, 56, 541, DateTimeKind.Unspecified).AddTicks(5313), "Odit sit qui voluptatum repellat.", 3, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 24, 21, 41, 5, 685, DateTimeKind.Unspecified).AddTicks(9180), "ipsum", null, "Praesentium dolores eos ipsa pariatur.", 19, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 21, 20, 6, 17, 243, DateTimeKind.Unspecified).AddTicks(2634), "eos", new DateTime(2018, 8, 8, 3, 21, 9, 352, DateTimeKind.Unspecified).AddTicks(4538), "Sed quos vitae occaecati repellat.", 9, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 7, 22, 12, 54, 58, 662, DateTimeKind.Unspecified).AddTicks(9181), "Dolorem asperiores cum voluptatem ea accusamus eos impedit perspiciatis rerum.\nSed non maxime sed similique numquam iure et.\nVitae autem dolor dolore nesciunt accusamus fugiat cupiditate.\nError sapiente accusantium.\nOdit eaque et voluptate quis mollitia veniam quisquam.", null, "Sit libero ipsum facere officia.", 12, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 4, 2, 36, 35, 548, DateTimeKind.Unspecified).AddTicks(2780), "distinctio", "Et ad omnis id ut.", 12, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 14, 13, 4, 46, 804, DateTimeKind.Unspecified).AddTicks(2465), "Temporibus qui impedit et neque voluptas autem quidem. Voluptate aspernatur repellendus. Porro et iste. Qui voluptatem voluptas commodi est aliquam distinctio nobis aut vel. Dolore aut delectus. Voluptatum velit et nihil error recusandae odio optio.", null, "Doloribus provident eaque et impedit.", 29, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 24, 12, 4, 40, 768, DateTimeKind.Unspecified).AddTicks(5637), "Nesciunt commodi sit error quae recusandae amet praesentium est reprehenderit.\nOdio iure nemo eligendi veritatis sed asperiores aperiam a quia.\nLaboriosam vel consequatur iure expedita.", new DateTime(2020, 4, 16, 11, 29, 55, 105, DateTimeKind.Unspecified).AddTicks(6829), "Doloremque quia ad et deleniti.", 5, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 2, 8, 22, 47, 56, 384, DateTimeKind.Unspecified).AddTicks(9864), "Ut soluta harum illo consequatur.\nEst et non cumque corrupti laborum ex harum et accusantium.", new DateTime(2019, 3, 31, 5, 44, 4, 205, DateTimeKind.Unspecified).AddTicks(3629), "Facere voluptatem ab quidem vero.", 20, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 5, 29, 4, 11, 32, 77, DateTimeKind.Unspecified).AddTicks(4307), "Ut voluptatum aperiam nihil sunt tempore. Omnis aut et recusandae voluptatem saepe laudantium minima. Ullam quia sint accusantium et aut vero est porro. Sed consequatur mollitia eius aliquam minus maiores temporibus ex perferendis. Est perspiciatis magni voluptatem dignissimos hic neque.", "Error fuga blanditiis repellendus harum.", 14, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 7, 12, 2, 33, 35, 582, DateTimeKind.Unspecified).AddTicks(442), "Nemo maiores quia omnis optio neque qui. Velit aperiam odit odit. Voluptatem est in iure nihil.", "Laborum corrupti excepturi rem dolorum.", 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 5, 7, 1, 18, 29, 3, DateTimeKind.Unspecified).AddTicks(5490), "Quibusdam aut qui enim nam.\nIn dolorum veniam commodi sint.\nDeleniti ex asperiores vitae.", new DateTime(2018, 1, 5, 19, 22, 19, 88, DateTimeKind.Unspecified).AddTicks(6905), "Quam sit distinctio maiores minus.", 29, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 26, 7, 9, 31, 769, DateTimeKind.Unspecified).AddTicks(7398), "At quas recusandae libero.\nAsperiores accusamus asperiores maiores labore.", new DateTime(2018, 10, 27, 14, 10, 19, 771, DateTimeKind.Unspecified).AddTicks(802), "Sunt et molestiae repellendus temporibus.", 22, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 6, 12, 23, 51, 16, 281, DateTimeKind.Unspecified).AddTicks(5369), "Blanditiis autem voluptatem rem. Debitis omnis ipsa qui aut. Velit non vel quae repellat numquam aliquid. Optio minima eum ullam ullam ratione eligendi veritatis est. Laudantium provident a cumque possimus non sint. Qui est aut est assumenda et deserunt ad commodi voluptatem.", null, "Quisquam et accusantium in rerum.", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 12, 25, 12, 23, 42, 271, DateTimeKind.Unspecified).AddTicks(8879), "Vel eum non quia in iste quo blanditiis.\nNon accusantium illum cumque quod aut molestias.\nImpedit accusantium dolor libero sequi.", new DateTime(2020, 1, 30, 7, 22, 53, 953, DateTimeKind.Unspecified).AddTicks(514), "Quae odit eveniet fuga quos.", 4, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 5, 30, 1, 39, 31, 800, DateTimeKind.Unspecified).AddTicks(242));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2016, 10, 6, 11, 52, 55, 561, DateTimeKind.Unspecified).AddTicks(859), "Fish" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 10, 27, 1, 55, 16, 98, DateTimeKind.Unspecified).AddTicks(5978), "Shoes" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2018, 8, 6, 1, 19, 54, 982, DateTimeKind.Unspecified).AddTicks(5337), "Car" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2019, 12, 26, 5, 7, 17, 26, DateTimeKind.Unspecified).AddTicks(9557));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1960, 9, 26, 13, 50, 15, 216, DateTimeKind.Unspecified).AddTicks(8878), "Willard59@yahoo.com", "Willard", "Gleason", new DateTime(2020, 7, 5, 13, 48, 47, 836, DateTimeKind.Unspecified).AddTicks(3921) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 6, 25, 11, 23, 11, 588, DateTimeKind.Unspecified).AddTicks(9842), "Gayle_Cronin@yahoo.com", "Gayle", "Cronin", new DateTime(2019, 6, 28, 4, 16, 59, 618, DateTimeKind.Unspecified).AddTicks(8849), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1980, 11, 29, 20, 54, 54, 796, DateTimeKind.Unspecified).AddTicks(4601), "Tabitha76@gmail.com", "Tabitha", "Connelly", new DateTime(2017, 2, 7, 9, 55, 0, 401, DateTimeKind.Unspecified).AddTicks(248) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 6, 14, 23, 56, 16, 888, DateTimeKind.Unspecified).AddTicks(5968), "Guadalupe22@hotmail.com", "Guadalupe", "Weissnat", new DateTime(2017, 7, 15, 4, 50, 59, 812, DateTimeKind.Unspecified).AddTicks(4868), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 8, 4, 23, 4, 13, 25, DateTimeKind.Unspecified).AddTicks(4750), "Madeline.Pouros@gmail.com", "Madeline", "Pouros", new DateTime(2019, 5, 22, 19, 56, 28, 452, DateTimeKind.Unspecified).AddTicks(4918), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2000, 10, 19, 0, 14, 52, 877, DateTimeKind.Unspecified).AddTicks(4346), "Sadie.Rau36@gmail.com", "Sadie", "Rau", new DateTime(2016, 12, 3, 9, 29, 54, 114, DateTimeKind.Unspecified).AddTicks(9275), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1997, 3, 21, 11, 41, 49, 509, DateTimeKind.Unspecified).AddTicks(5354), "Ken_Johnson@yahoo.com", "Ken", "Johnson", new DateTime(2020, 9, 2, 20, 2, 28, 9, DateTimeKind.Unspecified).AddTicks(4634) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 2, 14, 3, 44, 25, 272, DateTimeKind.Unspecified).AddTicks(7756), "Emanuel_Bruen87@gmail.com", "Emanuel", "Bruen", new DateTime(2017, 9, 7, 5, 44, 4, 305, DateTimeKind.Unspecified).AddTicks(5353), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1962, 8, 20, 14, 8, 20, 931, DateTimeKind.Unspecified).AddTicks(8201), "Ben_Reilly81@hotmail.com", "Ben", "Reilly", new DateTime(2019, 5, 8, 3, 31, 56, 389, DateTimeKind.Unspecified).AddTicks(3699), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 8, 8, 8, 53, 49, 304, DateTimeKind.Unspecified).AddTicks(5000), "Darren.Olson75@hotmail.com", "Darren", "Olson", new DateTime(2019, 6, 4, 9, 38, 12, 935, DateTimeKind.Unspecified).AddTicks(1149), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1965, 1, 12, 23, 54, 11, 944, DateTimeKind.Unspecified).AddTicks(5552), "Robin_West@yahoo.com", "Robin", "West", new DateTime(2020, 5, 13, 16, 18, 28, 43, DateTimeKind.Unspecified).AddTicks(7725), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1963, 6, 13, 11, 12, 49, 660, DateTimeKind.Unspecified).AddTicks(3918), "Carl_Smitham@yahoo.com", "Carl", "Smitham", new DateTime(2017, 9, 20, 22, 4, 40, 285, DateTimeKind.Unspecified).AddTicks(3671), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 1, 5, 5, 13, 49, 97, DateTimeKind.Unspecified).AddTicks(9996), "Ramiro.Ortiz10@hotmail.com", "Ramiro", "Ortiz", new DateTime(2019, 1, 16, 0, 43, 39, 988, DateTimeKind.Unspecified).AddTicks(2847), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1979, 9, 6, 10, 5, 45, 966, DateTimeKind.Unspecified).AddTicks(4231), "Jan91@hotmail.com", "Jan", "Treutel", new DateTime(2018, 12, 30, 4, 46, 22, 919, DateTimeKind.Unspecified).AddTicks(5207), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 2, 7, 38, 51, 529, DateTimeKind.Unspecified).AddTicks(2562), "Vivian.Hodkiewicz@hotmail.com", "Vivian", "Hodkiewicz", new DateTime(2019, 12, 6, 20, 12, 56, 306, DateTimeKind.Unspecified).AddTicks(357), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1988, 3, 14, 3, 28, 28, 58, DateTimeKind.Unspecified).AddTicks(4941), "Lionel.Dietrich@hotmail.com", "Lionel", "Dietrich", new DateTime(2018, 8, 22, 10, 2, 51, 374, DateTimeKind.Unspecified).AddTicks(890), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1973, 2, 9, 8, 3, 59, 60, DateTimeKind.Unspecified).AddTicks(7298), "Elizabeth70@hotmail.com", "Elizabeth", "Hyatt", new DateTime(2020, 4, 18, 23, 29, 12, 0, DateTimeKind.Unspecified).AddTicks(852), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 8, 11, 4, 1, 1, 494, DateTimeKind.Unspecified).AddTicks(1680), "Betsy_Gleason@gmail.com", "Betsy", "Gleason", new DateTime(2019, 12, 12, 10, 48, 30, 617, DateTimeKind.Unspecified).AddTicks(478), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1978, 4, 22, 9, 30, 23, 463, DateTimeKind.Unspecified).AddTicks(9133), "Kim.Carter@yahoo.com", "Kim", "Carter", new DateTime(2019, 4, 24, 16, 56, 0, 864, DateTimeKind.Unspecified).AddTicks(1834), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1977, 12, 23, 23, 18, 45, 416, DateTimeKind.Unspecified).AddTicks(1973), "Brett_Larkin64@hotmail.com", "Brett", "Larkin", new DateTime(2018, 8, 12, 15, 1, 59, 717, DateTimeKind.Unspecified).AddTicks(3400) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1980, 8, 19, 12, 21, 3, 983, DateTimeKind.Unspecified).AddTicks(3478), "Leslie.Jacobson@hotmail.com", "Leslie", "Jacobson", new DateTime(2016, 10, 2, 15, 10, 27, 663, DateTimeKind.Unspecified).AddTicks(865), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2009, 2, 5, 2, 34, 44, 921, DateTimeKind.Unspecified).AddTicks(1974), "Andrea8@gmail.com", "Andrea", "Moore", new DateTime(2018, 12, 24, 14, 33, 43, 22, DateTimeKind.Unspecified).AddTicks(5390), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1981, 12, 19, 4, 7, 18, 894, DateTimeKind.Unspecified).AddTicks(1328), "Darrell.Hills39@gmail.com", "Darrell", "Hills", new DateTime(2018, 4, 25, 18, 44, 27, 908, DateTimeKind.Unspecified).AddTicks(3527), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1978, 8, 17, 19, 57, 7, 297, DateTimeKind.Unspecified).AddTicks(5034), "Oliver_Wyman@gmail.com", "Oliver", "Wyman", new DateTime(2016, 12, 6, 17, 54, 3, 445, DateTimeKind.Unspecified).AddTicks(7379), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 8, 14, 17, 33, 20, 498, DateTimeKind.Unspecified).AddTicks(9084), "Cedric_Ortiz@yahoo.com", "Cedric", "Ortiz", new DateTime(2017, 4, 1, 1, 41, 35, 456, DateTimeKind.Unspecified).AddTicks(5002), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 3, 24, 3, 35, 57, 553, DateTimeKind.Unspecified).AddTicks(3004), "Wendy_Gleason98@hotmail.com", "Wendy", "Gleason", new DateTime(2018, 12, 14, 17, 25, 14, 178, DateTimeKind.Unspecified).AddTicks(7136), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1989, 2, 23, 6, 41, 14, 103, DateTimeKind.Unspecified).AddTicks(6302), "Annie15@gmail.com", "Annie", "Bode", new DateTime(2018, 4, 28, 23, 17, 36, 224, DateTimeKind.Unspecified).AddTicks(4180), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 10, 17, 15, 8, 1, 857, DateTimeKind.Unspecified).AddTicks(376), "Elvira.Schaefer@gmail.com", "Elvira", "Schaefer", new DateTime(2020, 10, 24, 19, 48, 4, 786, DateTimeKind.Unspecified).AddTicks(5854), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1972, 5, 3, 19, 6, 11, 8, DateTimeKind.Unspecified).AddTicks(3060), "Marie_Kreiger24@gmail.com", "Marie", "Kreiger", new DateTime(2020, 12, 29, 8, 30, 39, 901, DateTimeKind.Unspecified).AddTicks(518), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 11, 14, 12, 48, 42, 625, DateTimeKind.Unspecified).AddTicks(7300), "Stacey_Howell@yahoo.com", "Stacey", "Howell", new DateTime(2017, 5, 1, 0, 9, 7, 356, DateTimeKind.Unspecified).AddTicks(3554), 5 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RegisteredAt",
                table: "Users",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldDefaultValueSql: "GETDATE()");

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(64)");

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(64)");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(64)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "BirthDay",
                table: "Users",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "Teams",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldDefaultValueSql: "GETDATE()");

            migrationBuilder.AlterColumn<string>(
                name: "TaskName",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FinishedAt",
                table: "Tasks",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(512)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "Tasks",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldDefaultValueSql: "GETDATE()");

            migrationBuilder.AlterColumn<string>(
                name: "ProjectName",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(512)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Deadline",
                table: "Projects",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedAt",
                table: "Projects",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldDefaultValueSql: "GETDATE()");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 14, new DateTime(2019, 8, 1, 21, 24, 33, 529, DateTimeKind.Unspecified).AddTicks(2391), new DateTime(2022, 1, 1, 19, 49, 6, 517, DateTimeKind.Local).AddTicks(4262), "Voluptatem est impedit iure repellendus dignissimos quo amet voluptatem soluta dolorem quia sit aut eos.", "Dolorem nemo architecto sit amet est.", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 24, new DateTime(2017, 10, 15, 10, 4, 18, 662, DateTimeKind.Unspecified).AddTicks(7727), new DateTime(2022, 9, 10, 5, 16, 55, 925, DateTimeKind.Local).AddTicks(5028), "Et perferendis nihil et et id in qui perspiciatis earum ipsam magnam inventore laboriosam aut.", "Maiores et ipsum ut molestias eligendi.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 15, new DateTime(2019, 11, 19, 21, 42, 21, 613, DateTimeKind.Unspecified).AddTicks(1667), new DateTime(2022, 2, 19, 15, 28, 21, 973, DateTimeKind.Local).AddTicks(8732), "Distinctio et quas velit ratione sunt voluptatem velit optio aliquid et aut quia tempora rerum.", "Deleniti aut doloribus aut velit quos.", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 11, new DateTime(2017, 3, 16, 12, 47, 5, 619, DateTimeKind.Unspecified).AddTicks(2153), new DateTime(2021, 10, 2, 2, 8, 45, 692, DateTimeKind.Local).AddTicks(7440), "Corporis qui laborum doloremque officia odio voluptates molestias quis voluptatem sed eveniet aliquam qui at.", "Perspiciatis vitae officia sed similique consectetur.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 29, new DateTime(2019, 1, 6, 10, 0, 45, 298, DateTimeKind.Unspecified).AddTicks(478), new DateTime(2022, 4, 13, 4, 29, 47, 200, DateTimeKind.Local).AddTicks(1209), "Eum sint earum et voluptate architecto et recusandae consequatur voluptas optio sed ipsum odit voluptates.", "Soluta repellendus officiis omnis reiciendis illo.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 8, new DateTime(2020, 11, 11, 0, 37, 20, 708, DateTimeKind.Unspecified).AddTicks(9095), new DateTime(2022, 6, 6, 13, 49, 7, 602, DateTimeKind.Local).AddTicks(9636), "Aperiam modi magni necessitatibus voluptate et voluptas harum aut sed et delectus at sapiente amet.", "Eum non totam beatae nemo autem.", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 6, new DateTime(2020, 8, 11, 4, 45, 23, 503, DateTimeKind.Unspecified).AddTicks(7094), new DateTime(2022, 10, 13, 4, 23, 24, 277, DateTimeKind.Local).AddTicks(1410), "Minima et qui voluptatem non sint odio harum minima autem pariatur consequatur quae mollitia velit.", "Sequi harum aut aliquam beatae neque.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 18, new DateTime(2018, 5, 3, 2, 13, 11, 439, DateTimeKind.Unspecified).AddTicks(5384), new DateTime(2022, 3, 24, 21, 15, 52, 111, DateTimeKind.Local).AddTicks(3368), "Natus eius cupiditate sequi maxime voluptatem ut illo est consequuntur molestias aliquam atque ducimus quis.", "Maiores omnis est maiores omnis non." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 10, new DateTime(2018, 4, 8, 0, 13, 25, 522, DateTimeKind.Unspecified).AddTicks(1248), new DateTime(2022, 10, 28, 11, 44, 14, 614, DateTimeKind.Local).AddTicks(5524), "Qui nobis aliquid sunt est et recusandae consequatur earum delectus optio repudiandae deleniti provident modi.", "Suscipit magnam et est qui ullam." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 17, new DateTime(2018, 10, 18, 19, 57, 16, 568, DateTimeKind.Unspecified).AddTicks(8271), new DateTime(2022, 5, 11, 8, 9, 17, 404, DateTimeKind.Local).AddTicks(1547), "Eius architecto in dignissimos a rerum similique eum at sit totam et sapiente sed dolores.", "Et mollitia quam placeat et quas.", 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 4, 23, 0, 47, 26, 165, DateTimeKind.Unspecified).AddTicks(9758), "Id sit ut nihil enim itaque minus officia reprehenderit iste.", null, "Officia similique at ipsa commodi.", 30, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 20, 7, 5, 45, 768, DateTimeKind.Unspecified).AddTicks(1089), "Ut suscipit voluptatem debitis.", null, "Provident et sint possimus voluptatem.", 28, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 6, 29, 21, 34, 21, 419, DateTimeKind.Unspecified).AddTicks(5530), "voluptatem", new DateTime(2021, 4, 19, 4, 49, 12, 288, DateTimeKind.Unspecified).AddTicks(1097), "Eaque doloribus accusantium eaque iure.", 27, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 27, 23, 53, 31, 652, DateTimeKind.Unspecified).AddTicks(3979), "modi", "Eaque dicta iste sequi praesentium.", 6, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 26, 18, 58, 50, 738, DateTimeKind.Unspecified).AddTicks(5554), "Cupiditate dolores autem.\nQuis maxime iure deleniti consequatur tempora vel voluptatibus rem doloremque.\nEt voluptatem fugit similique saepe.\nEt iusto et dignissimos.\nDolor et deserunt ad eos quia porro nihil.", new DateTime(2020, 7, 3, 13, 22, 27, 531, DateTimeKind.Unspecified).AddTicks(4448), "Non ut iste et libero.", 22, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 27, 9, 4, 1, 23, DateTimeKind.Unspecified).AddTicks(4916), "Id quos aut omnis.\nOmnis aut accusantium veritatis.\nExcepturi et error qui rerum provident commodi.\nUt assumenda laborum possimus.\nQuisquam magnam at et qui.\nInventore doloribus nulla nihil repellat rerum aut dolorem enim.", new DateTime(2018, 6, 23, 17, 21, 14, 252, DateTimeKind.Unspecified).AddTicks(463), "Et maxime quisquam vel consequatur.", 25, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 6, 18, 24, 22, 567, DateTimeKind.Unspecified).AddTicks(2336), "perferendis", new DateTime(2018, 6, 23, 13, 44, 22, 64, DateTimeKind.Unspecified).AddTicks(3060), "Dicta non voluptatem molestias vero.", 5, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 9, 2, 37, 55, 657, DateTimeKind.Unspecified).AddTicks(1173), "alias", new DateTime(2020, 9, 26, 7, 1, 40, 791, DateTimeKind.Unspecified).AddTicks(4571), "Reprehenderit corporis quis occaecati atque.", 29, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 25, 1, 23, 51, 124, DateTimeKind.Unspecified).AddTicks(8758), "ad", new DateTime(2019, 11, 19, 18, 12, 12, 667, DateTimeKind.Unspecified).AddTicks(8959), "Consequatur illo dolorem enim rerum.", 2, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 1, 8, 16, 12, 49, 608, DateTimeKind.Unspecified).AddTicks(4324), "Ut omnis consequatur placeat iusto laboriosam ratione deleniti omnis.", null, "Officia ipsa iure excepturi perspiciatis.", 18, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 11, 18, 28, 49, 825, DateTimeKind.Unspecified).AddTicks(881), "Sint possimus voluptatem qui repellendus rerum expedita.", "Dolorem a iusto voluptatem numquam.", 5, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 26, 19, 51, 30, 131, DateTimeKind.Unspecified).AddTicks(7602), "Quibusdam nam numquam. Sed qui praesentium voluptas vitae hic quos voluptas sunt. Ab sit quod neque. Reprehenderit corrupti sint ex voluptatem eos ducimus nostrum. Ut repudiandae sit ratione laborum.", new DateTime(2019, 8, 17, 13, 36, 38, 289, DateTimeKind.Unspecified).AddTicks(4892), "Quo vel officia quae quam.", 8, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 7, 15, 19, 55, 351, DateTimeKind.Unspecified).AddTicks(4177), "Quas omnis similique.\nAliquam modi numquam qui minima.", null, "Illo corrupti saepe debitis dolore.", 28, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 6, 15, 0, 37, 48, 676, DateTimeKind.Unspecified).AddTicks(7320), "Et deleniti magnam cumque mollitia et.\nRerum odit id temporibus voluptate temporibus et.\nEst omnis et qui soluta quas earum officia.\nAutem et nostrum consequatur nihil ipsam voluptatem.\nEius quasi sapiente sunt illo.\nEt nemo doloribus quidem sed ut quia.", new DateTime(2018, 8, 7, 10, 48, 8, 299, DateTimeKind.Unspecified).AddTicks(4786), "Adipisci non ipsum recusandae nisi.", 21, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 4, 5, 3, 15, 15, 437, DateTimeKind.Unspecified).AddTicks(5667), "Voluptatibus et incidunt recusandae autem veniam et amet porro ut. Iste quia exercitationem sunt eligendi et quod. Praesentium provident delectus mollitia. Qui ratione eveniet eaque beatae qui odit. Eum est facilis est.", new DateTime(2018, 4, 8, 16, 42, 3, 173, DateTimeKind.Unspecified).AddTicks(4677), "Quam voluptatem eos molestiae vero.", 13, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 19, 0, 59, 19, 789, DateTimeKind.Unspecified).AddTicks(6250), "incidunt", "Recusandae sequi quis consequatur delectus.", 4, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 9, 9, 21, 36, 24, 409, DateTimeKind.Unspecified).AddTicks(6725), "Odit in iusto eum quia iste.\nIn recusandae dolorem sit quia assumenda qui aperiam.\nNumquam eos dolor est aut harum.", null, "Autem perferendis voluptatem qui autem.", 17, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 3, 11, 24, 38, 70, DateTimeKind.Unspecified).AddTicks(7118), "Ex quisquam et fugiat velit rem eum excepturi saepe temporibus.\nOdio et voluptatibus culpa excepturi expedita.\nLaborum officia voluptatum quos ducimus nulla.", new DateTime(2018, 11, 24, 14, 35, 10, 802, DateTimeKind.Unspecified).AddTicks(6090), "Harum libero doloremque voluptates voluptas.", 1, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 18, 10, 27, 36, 935, DateTimeKind.Unspecified).AddTicks(2534), "Assumenda ullam reiciendis sed consectetur.\nIllo minus aliquid est.\nOccaecati aliquam exercitationem.", "Similique dolores voluptatum atque cum.", 15, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 23, 11, 21, 15, 638, DateTimeKind.Unspecified).AddTicks(2552), "Maiores delectus iusto corporis.\nSed aliquid cumque ipsam est ea corporis dolore autem.\nNon quis rerum eum alias.\nEt voluptas asperiores consequatur dignissimos.\nTotam quidem pariatur debitis molestias.", new DateTime(2019, 3, 19, 22, 7, 27, 799, DateTimeKind.Unspecified).AddTicks(6557), "Inventore fugiat magni qui aspernatur.", 2, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 6, 0, 37, 47, 881, DateTimeKind.Unspecified).AddTicks(3632), "dolores", null, "Incidunt ipsam ut mollitia placeat.", 9, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 2, 17, 44, 1, 443, DateTimeKind.Unspecified).AddTicks(1288), "Quos velit hic aut veritatis laudantium.\nAt facere ut est quas perferendis voluptatem voluptate.\nAspernatur tempore nobis.", new DateTime(2021, 3, 2, 14, 7, 34, 336, DateTimeKind.Unspecified).AddTicks(347), "Vitae praesentium tempora quidem hic.", 8, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 11, 23, 35, 53, 371, DateTimeKind.Unspecified).AddTicks(3945), "excepturi", "Iusto voluptas esse minima repudiandae.", 26, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 6, 26, 12, 10, 59, 99, DateTimeKind.Unspecified).AddTicks(4026), "Veritatis amet inventore illo nesciunt ut ad. Provident qui et asperiores delectus reiciendis fuga cumque ipsam. Quo labore numquam earum perspiciatis ipsum officia. Beatae eos in corporis in odio expedita dignissimos. Dolore nihil commodi quia autem possimus rerum doloremque rerum. Dolorem in ipsa repudiandae qui ab quo.", new DateTime(2021, 2, 12, 1, 7, 54, 362, DateTimeKind.Unspecified).AddTicks(4851), "Voluptatem sed veniam consequatur veniam.", 10, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 7, 16, 2, 52, 15, 108, DateTimeKind.Unspecified).AddTicks(4206), "aut", "Totam blanditiis praesentium eos totam.", 16, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 18, 6, 32, 32, 836, DateTimeKind.Unspecified).AddTicks(1698), "Natus voluptas corporis aperiam tempora vel magnam et.\nMagni sed est id illum saepe.\nRepudiandae et ipsum et officiis aut atque officiis.\nUt deserunt aspernatur voluptatem excepturi ipsa facere non doloribus sit.\nSequi molestiae repudiandae et reiciendis ratione laboriosam quia.\nVelit est dolor inventore repellendus unde.", new DateTime(2018, 12, 24, 12, 0, 26, 929, DateTimeKind.Unspecified).AddTicks(4770), "Asperiores tenetur ducimus laudantium atque.", 4, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 4, 8, 23, 3, 667, DateTimeKind.Unspecified).AddTicks(3564), "Consequatur excepturi et.\nConsectetur molestias corrupti nihil sint qui.\nLaudantium ad totam dolores assumenda velit eaque voluptatum.\nDolor aliquid aut quia sunt nihil est ullam tempora et.\nPerspiciatis eum fugit ipsa ut sequi eligendi accusamus ad.\nNesciunt aut nostrum.", new DateTime(2021, 3, 21, 20, 8, 23, 126, DateTimeKind.Unspecified).AddTicks(6714), "Molestiae consequuntur est modi sequi.", 1, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 17, 16, 5, 36, 933, DateTimeKind.Unspecified).AddTicks(3701), "voluptate", "Consequatur sed consequuntur asperiores repudiandae.", 23, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 9, 4, 4, 47, 3, 701, DateTimeKind.Unspecified).AddTicks(2750), "molestiae", null, "Deleniti dicta asperiores sit beatae.", 22, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 12, 19, 7, 59, 22, 384, DateTimeKind.Unspecified).AddTicks(4996), "Voluptas libero nemo dolorum impedit quam.", new DateTime(2020, 9, 24, 17, 19, 8, 859, DateTimeKind.Unspecified).AddTicks(8414), "Ut voluptatem totam vel voluptatem.", 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 1, 12, 19, 42, 45, 268, DateTimeKind.Unspecified).AddTicks(6643), "Numquam voluptas provident aperiam nemo sunt hic molestiae sunt.", new DateTime(2019, 3, 15, 16, 42, 0, 782, DateTimeKind.Unspecified).AddTicks(8778), "Aut dolores accusamus cum quibusdam.", 18, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 15, 4, 44, 46, 948, DateTimeKind.Unspecified).AddTicks(3252), "In odit et earum et nihil doloremque nisi tempore occaecati. Quasi culpa incidunt quidem repellat. Omnis quos ullam enim deleniti saepe corrupti. Omnis deleniti aliquid et. Aut aut cupiditate rerum dolores.", "Perferendis occaecati quia totam quis.", 5, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 6, 10, 44, 24, 264, DateTimeKind.Unspecified).AddTicks(6925), "Aut dolor recusandae cupiditate enim iure facilis et non.\nTemporibus corporis tempora soluta nulla nisi.\nDolor perspiciatis eligendi aspernatur impedit omnis eum.\nAd labore voluptas id illum excepturi officia repellat optio totam.\nQui atque fugiat eos et.", new DateTime(2021, 6, 24, 2, 12, 41, 281, DateTimeKind.Unspecified).AddTicks(495), "Inventore unde officiis beatae aut.", 2, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 16, 20, 9, 38, 627, DateTimeKind.Unspecified).AddTicks(33), "quasi", new DateTime(2021, 6, 30, 5, 47, 5, 716, DateTimeKind.Unspecified).AddTicks(138), "Vel cumque provident similique quibusdam.", 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 10, 17, 11, 2, 35, 858, DateTimeKind.Unspecified).AddTicks(7721), "nemo", null, "Ratione debitis voluptatem qui et.", 16, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 14, 18, 3, 28, 745, DateTimeKind.Unspecified).AddTicks(7900), "Exercitationem minima eos.", new DateTime(2021, 4, 2, 10, 56, 32, 786, DateTimeKind.Unspecified).AddTicks(4830), "Ut voluptate qui necessitatibus asperiores.", 8, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 11, 12, 22, 59, 0, 162, DateTimeKind.Unspecified).AddTicks(2353), "Saepe quia sed sequi ad. Dolorem dolor qui ut ducimus est. Autem dolor eos cupiditate aliquid porro quis. Dolore qui ratione harum autem occaecati sint eum odit. Sint quam dicta repellat. Quis officiis voluptatem illum eligendi necessitatibus corrupti impedit.", null, "Aut exercitationem optio nulla fugiat.", 10, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 2, 26, 23, 18, 22, 480, DateTimeKind.Unspecified).AddTicks(6145), "Illo cupiditate eveniet earum.\nUt deleniti ut recusandae consectetur.\nQuibusdam molestias at et sit eveniet mollitia.\nOmnis iusto debitis libero et corporis et enim repellendus quo.\nPorro sed ex dolores incidunt ut.", null, "Natus voluptatem deleniti fugit cumque.", 14, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 6, 20, 14, 6, 47, 595, DateTimeKind.Unspecified).AddTicks(9353), "Voluptatem numquam odit et aut quod dolor at.\nQuidem ducimus beatae eligendi quidem sed voluptatum asperiores dignissimos.\nFuga corporis quia.\nSequi iure possimus.\nPerspiciatis ut minima.", new DateTime(2018, 8, 22, 9, 2, 9, 367, DateTimeKind.Unspecified).AddTicks(8261), "Ea suscipit assumenda repellendus quam.", 17, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 23, 22, 2, 49, 370, DateTimeKind.Unspecified).AddTicks(8610), "Labore doloribus praesentium enim est.", new DateTime(2018, 4, 16, 20, 55, 36, 910, DateTimeKind.Unspecified).AddTicks(7103), "Est voluptates rem facere ab.", 24, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 6, 7, 42, 18, 796, DateTimeKind.Unspecified).AddTicks(1264), "nam", null, "Consequatur earum consectetur hic labore.", 17, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 9, 5, 18, 37, 43, 204, DateTimeKind.Unspecified).AddTicks(3316), "ut", new DateTime(2018, 7, 29, 5, 51, 30, 918, DateTimeKind.Unspecified).AddTicks(6958), "Sint in architecto aliquid et.", 25, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 17, 13, 48, 15, 409, DateTimeKind.Unspecified).AddTicks(5312), "Id deleniti placeat dolorum doloribus expedita libero ut dicta esse.", new DateTime(2020, 7, 28, 4, 55, 56, 816, DateTimeKind.Unspecified).AddTicks(7188), "Molestiae dignissimos aliquid maxime est.", 15, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 14, 15, 19, 47, 352, DateTimeKind.Unspecified).AddTicks(5072), "Natus voluptatem voluptate temporibus ipsam nobis ipsam.", new DateTime(2018, 2, 13, 5, 27, 43, 884, DateTimeKind.Unspecified).AddTicks(4316), "Et ducimus molestiae impedit alias.", 2, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 28, 13, 41, 8, 1, DateTimeKind.Unspecified).AddTicks(8312), "Quaerat sit corporis nihil.\nImpedit cumque ullam sit dolor voluptates ad ut consectetur animi.", new DateTime(2019, 11, 5, 12, 0, 9, 449, DateTimeKind.Unspecified).AddTicks(9591), "Dolorem qui commodi quia esse.", 11, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 4, 5, 1, 49, 52, 354, DateTimeKind.Unspecified).AddTicks(6703), "Eius exercitationem autem. Ea corporis quo odit. Sit aut et deserunt nesciunt.", new DateTime(2020, 12, 25, 4, 37, 26, 977, DateTimeKind.Unspecified).AddTicks(5101), "Sed tempore facilis explicabo voluptates.", 12, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 8, 15, 1, 37, 36, 293, DateTimeKind.Unspecified).AddTicks(3636), "Distinctio sit quasi et et numquam sapiente.\nProvident quia laborum rerum omnis deleniti.", new DateTime(2018, 2, 26, 8, 10, 10, 442, DateTimeKind.Unspecified).AddTicks(3348), "Dolor laboriosam inventore sapiente ea.", 21, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 2, 18, 9, 7, 48, 852, DateTimeKind.Unspecified).AddTicks(5115), "Aut ut esse placeat minus.", null, "Reprehenderit impedit est nulla enim.", 14, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 1, 1, 22, 31, 7, 770, DateTimeKind.Unspecified).AddTicks(6217), "Qui placeat sunt ullam.", new DateTime(2018, 1, 15, 6, 21, 32, 721, DateTimeKind.Unspecified).AddTicks(1238), "Quia fugit vel adipisci quibusdam.", 9, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 2, 10, 14, 48, 405, DateTimeKind.Unspecified).AddTicks(824), "voluptates", new DateTime(2020, 3, 30, 6, 56, 43, 74, DateTimeKind.Unspecified).AddTicks(6438), "Sed hic est tempora provident.", 4, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 28, 7, 1, 39, 878, DateTimeKind.Unspecified).AddTicks(2335), "iste", null, "Iusto aperiam rem omnis sint.", 10, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 29, 6, 49, 46, 581, DateTimeKind.Unspecified).AddTicks(5188), "dolores", "Occaecati quaerat alias ab velit.", 7, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 12, 10, 32, 26, 464, DateTimeKind.Unspecified).AddTicks(3208), "In aut et ut ea ad possimus non eos.\nCupiditate quidem magni fugiat.\nEveniet voluptates aperiam.", null, "Aperiam rerum sapiente totam fugit.", 27, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 6, 2, 22, 35, 45, 404, DateTimeKind.Unspecified).AddTicks(1994), "dicta", new DateTime(2019, 6, 8, 1, 48, 0, 65, DateTimeKind.Unspecified).AddTicks(480), "Tempora sed quae qui officiis.", 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 27, 4, 31, 6, 104, DateTimeKind.Unspecified).AddTicks(1753), "Quasi nobis eos exercitationem velit et et natus vitae quos.", null, "Id minus eligendi et iure.", 23, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 1, 24, 1, 34, 28, 78, DateTimeKind.Unspecified).AddTicks(4118), "et", new DateTime(2019, 1, 10, 22, 46, 33, 515, DateTimeKind.Unspecified).AddTicks(6960), "Minus enim perspiciatis aut magni.", 16, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 27, 11, 9, 12, 365, DateTimeKind.Unspecified).AddTicks(9759), "Tempore facere non dolore perferendis cupiditate et. Porro est vitae nihil quia repellat eum. Quas sequi nulla rerum non dolore. Sit accusantium assumenda iure quod commodi quisquam ea autem.", null, "Quia laboriosam magni officia officia.", 2, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 19, 10, 19, 11, 738, DateTimeKind.Unspecified).AddTicks(9590), "Eveniet sed quia a facere vitae iusto explicabo eligendi sit. Voluptatibus ut modi et eos voluptates. Asperiores sed quis suscipit. Itaque dolorem commodi et eaque. Eos nostrum ex at magnam. Et non repellendus laborum.", new DateTime(2020, 3, 21, 15, 14, 56, 626, DateTimeKind.Unspecified).AddTicks(8252), "Quidem sed non dolor sint.", 22, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 19, 18, 31, 1, 12, DateTimeKind.Unspecified).AddTicks(8296), "Quo facilis error eum ut dolores iure numquam qui omnis. Ipsa quasi magni explicabo explicabo deserunt molestiae consequuntur culpa. Incidunt officia expedita minus nemo. Eligendi qui dolorem molestiae. Error praesentium eos est natus qui reiciendis est modi nihil.", new DateTime(2019, 7, 23, 13, 10, 2, 369, DateTimeKind.Unspecified).AddTicks(9625), "In libero esse officiis reprehenderit.", 26, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2017, 5, 24, 9, 0, 9, 270, DateTimeKind.Unspecified).AddTicks(9767), "ut", new DateTime(2021, 2, 6, 9, 4, 22, 260, DateTimeKind.Unspecified).AddTicks(3036), "In voluptatem veniam totam maxime.", 28, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 25, 7, 9, 5, 521, DateTimeKind.Unspecified).AddTicks(791), "Asperiores atque sed assumenda rerum voluptatum ea.", "Optio voluptas incidunt pariatur quo.", 20, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 22, 0, 6, 45, 684, DateTimeKind.Unspecified).AddTicks(4624), "Quam sint expedita omnis.", new DateTime(2020, 4, 21, 22, 35, 37, 264, DateTimeKind.Unspecified).AddTicks(8196), "Quod aut sed magni aut.", 2, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 18, 21, 36, 29, 887, DateTimeKind.Unspecified).AddTicks(8162), "Repellendus excepturi occaecati.", null, "Nisi beatae aut aperiam tenetur.", 19, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 26, 18, 9, 24, 183, DateTimeKind.Unspecified).AddTicks(4116), "Deserunt veritatis eligendi suscipit recusandae rerum.\nEveniet quo eum.\nEnim qui quibusdam amet temporibus.", new DateTime(2021, 6, 5, 9, 50, 38, 677, DateTimeKind.Unspecified).AddTicks(6734), "Aut debitis amet vitae in.", 11, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 4, 10, 19, 5, 40, 278, DateTimeKind.Unspecified).AddTicks(3982), "Hic accusantium repellat est esse aut quaerat sed. Eveniet totam dolorum iste. Sit excepturi consequatur corporis eligendi in qui omnis quis reiciendis. Laborum molestiae eum et est laboriosam. Eaque doloremque debitis magnam. Saepe vel numquam non ut.", "Harum enim fugiat nesciunt unde.", 28, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2017, 2, 1, 22, 5, 0, 723, DateTimeKind.Unspecified).AddTicks(354), "laudantium", "Cum reiciendis quia aliquid et.", 23, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 7, 20, 11, 5, 419, DateTimeKind.Unspecified).AddTicks(1296), "Cupiditate beatae vitae velit repudiandae et voluptatum.\nDolores soluta voluptas quasi magni consectetur facere ea dolor.\nAspernatur quae in beatae deleniti excepturi.", new DateTime(2020, 3, 1, 9, 17, 21, 899, DateTimeKind.Unspecified).AddTicks(9174), "Aut perspiciatis debitis et eum.", 18, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 15, 23, 31, 3, 583, DateTimeKind.Unspecified).AddTicks(2257), "Iusto voluptatem cum.\nBlanditiis quisquam eveniet hic aut laborum temporibus.\nIusto delectus laudantium modi aut ducimus.\nSimilique et quaerat ut aut numquam debitis.\nTenetur iusto qui exercitationem.\nQuas inventore voluptatibus omnis.", new DateTime(2019, 3, 15, 6, 44, 33, 711, DateTimeKind.Unspecified).AddTicks(6434), "Atque rerum quidem minus eveniet.", 19, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 8, 16, 40, 2, 524, DateTimeKind.Unspecified).AddTicks(3398), "Quisquam ducimus repellat sit eius dolor.\nAccusamus autem in occaecati earum sint quo voluptatem harum ut.\nVoluptatibus nobis at doloremque dolorem quo.\nError porro vel molestiae eaque optio cum provident sit tempore.", new DateTime(2020, 10, 27, 19, 36, 3, 974, DateTimeKind.Unspecified).AddTicks(166), "Perspiciatis delectus aut dolor sit.", 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 5, 21, 11, 48, 42, 457, DateTimeKind.Unspecified).AddTicks(8087), "Dolore non aut ut id error eligendi fuga. Autem expedita praesentium deserunt. Laboriosam aut nulla velit dolores eaque voluptatum voluptatum quibusdam ut. Debitis ex nihil omnis. Veniam minus necessitatibus id cum dolorem saepe. Autem blanditiis voluptatibus eius.", null, "Dolores id quae labore tempore.", 6, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2017, 12, 5, 22, 18, 49, 13, DateTimeKind.Unspecified).AddTicks(7347));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 6, 19, 12, 46, 13, 622, DateTimeKind.Unspecified).AddTicks(1806), "Salad" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 2, 24, 10, 22, 21, 722, DateTimeKind.Unspecified).AddTicks(2472), "Soap" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2018, 10, 27, 22, 30, 33, 117, DateTimeKind.Unspecified).AddTicks(2758), "Tuna" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedAt",
                value: new DateTime(2016, 12, 10, 0, 45, 49, 148, DateTimeKind.Unspecified).AddTicks(6875));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1991, 5, 31, 15, 28, 15, 359, DateTimeKind.Unspecified).AddTicks(5690), "Todd.Effertz93@gmail.com", "Todd", "Effertz", new DateTime(2016, 1, 21, 10, 18, 15, 518, DateTimeKind.Unspecified).AddTicks(5839) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1978, 10, 12, 21, 21, 49, 112, DateTimeKind.Unspecified).AddTicks(3478), "Frances_Champlin60@yahoo.com", "Frances", "Champlin", new DateTime(2019, 2, 3, 4, 32, 20, 433, DateTimeKind.Unspecified).AddTicks(6052), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1969, 11, 20, 4, 15, 21, 121, DateTimeKind.Unspecified).AddTicks(486), "Dolores_Wuckert61@hotmail.com", "Dolores", "Wuckert", new DateTime(2018, 7, 7, 14, 11, 36, 102, DateTimeKind.Unspecified).AddTicks(7190) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 12, 8, 5, 40, 31, 200, DateTimeKind.Unspecified).AddTicks(8372), "Kristina.Fadel18@yahoo.com", "Kristina", "Fadel", new DateTime(2020, 9, 16, 23, 36, 16, 972, DateTimeKind.Unspecified).AddTicks(3930), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1960, 5, 29, 22, 27, 30, 692, DateTimeKind.Unspecified).AddTicks(4991), "Marshall.Schmidt40@yahoo.com", "Marshall", "Schmidt", new DateTime(2019, 4, 19, 21, 59, 39, 252, DateTimeKind.Unspecified).AddTicks(8417), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1960, 1, 3, 10, 58, 29, 168, DateTimeKind.Unspecified).AddTicks(9815), "Homer93@yahoo.com", "Homer", "Reynolds", new DateTime(2017, 12, 15, 17, 4, 9, 954, DateTimeKind.Unspecified).AddTicks(5905), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2010, 4, 13, 6, 24, 26, 404, DateTimeKind.Unspecified).AddTicks(6902), "Jamie_Jerde@hotmail.com", "Jamie", "Jerde", new DateTime(2016, 9, 23, 6, 57, 27, 784, DateTimeKind.Unspecified).AddTicks(466) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1986, 7, 12, 9, 17, 28, 335, DateTimeKind.Unspecified).AddTicks(9507), "Jared95@gmail.com", "Jared", "Schinner", new DateTime(2018, 2, 17, 3, 43, 16, 808, DateTimeKind.Unspecified).AddTicks(6528), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 8, 8, 3, 7, 16, 700, DateTimeKind.Unspecified).AddTicks(6340), "Meghan_Padberg@hotmail.com", "Meghan", "Padberg", new DateTime(2017, 2, 21, 23, 33, 39, 760, DateTimeKind.Unspecified).AddTicks(1296), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 3, 23, 13, 52, 43, 559, DateTimeKind.Unspecified).AddTicks(324), "Lynn.Collins67@hotmail.com", "Lynn", "Collins", new DateTime(2016, 11, 4, 3, 23, 40, 691, DateTimeKind.Unspecified).AddTicks(9980), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1961, 12, 13, 23, 35, 1, 850, DateTimeKind.Unspecified).AddTicks(7727), "Patty.Muller31@gmail.com", "Patty", "Muller", new DateTime(2019, 8, 10, 8, 31, 52, 646, DateTimeKind.Unspecified).AddTicks(7043), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1967, 7, 30, 16, 5, 49, 871, DateTimeKind.Unspecified).AddTicks(1796), "Luz.McClure46@gmail.com", "Luz", "McClure", new DateTime(2018, 5, 13, 12, 19, 26, 215, DateTimeKind.Unspecified).AddTicks(5418), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1980, 1, 18, 22, 19, 9, 783, DateTimeKind.Unspecified).AddTicks(9303), "Emanuel.Emmerich12@hotmail.com", "Emanuel", "Emmerich", new DateTime(2017, 9, 26, 9, 39, 6, 816, DateTimeKind.Unspecified).AddTicks(9389), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1981, 9, 24, 22, 34, 52, 817, DateTimeKind.Unspecified).AddTicks(1984), "Francis.Goyette16@yahoo.com", "Francis", "Goyette", new DateTime(2019, 8, 2, 12, 4, 32, 611, DateTimeKind.Unspecified).AddTicks(2553), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1982, 8, 23, 10, 18, 2, 295, DateTimeKind.Unspecified).AddTicks(2399), "Greg.Jerde@hotmail.com", "Greg", "Jerde", new DateTime(2018, 12, 31, 21, 39, 45, 764, DateTimeKind.Unspecified).AddTicks(1226), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1981, 6, 6, 18, 25, 50, 843, DateTimeKind.Unspecified).AddTicks(7615), "Jamie_Schmidt77@gmail.com", "Jamie", "Schmidt", new DateTime(2017, 10, 18, 9, 12, 46, 195, DateTimeKind.Unspecified).AddTicks(549), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 1, 8, 12, 29, 26, 633, DateTimeKind.Unspecified).AddTicks(56), "Marion6@gmail.com", "Marion", "Bradtke", new DateTime(2019, 2, 5, 18, 49, 13, 758, DateTimeKind.Unspecified).AddTicks(9046), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 6, 26, 13, 23, 16, 354, DateTimeKind.Unspecified).AddTicks(2538), "Karla_Krajcik31@hotmail.com", "Karla", "Krajcik", new DateTime(2016, 5, 2, 5, 6, 41, 953, DateTimeKind.Unspecified).AddTicks(2230), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1976, 6, 2, 1, 15, 41, 504, DateTimeKind.Unspecified).AddTicks(5178), "Rosie_Metz40@gmail.com", "Rosie", "Metz", new DateTime(2016, 7, 28, 6, 25, 49, 551, DateTimeKind.Unspecified).AddTicks(2519), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(1993, 3, 12, 9, 34, 12, 196, DateTimeKind.Unspecified).AddTicks(2074), "Becky21@hotmail.com", "Becky", "Weber", new DateTime(2020, 11, 9, 10, 28, 38, 603, DateTimeKind.Unspecified).AddTicks(4766) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 2, 24, 10, 31, 48, 119, DateTimeKind.Unspecified).AddTicks(1656), "Curtis0@hotmail.com", "Curtis", "McClure", new DateTime(2018, 11, 24, 10, 56, 56, 606, DateTimeKind.Unspecified).AddTicks(7799), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1980, 12, 27, 7, 41, 21, 328, DateTimeKind.Unspecified).AddTicks(2828), "Alice17@hotmail.com", "Alice", "Kreiger", new DateTime(2020, 6, 20, 2, 38, 0, 882, DateTimeKind.Unspecified).AddTicks(3153), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1972, 6, 24, 15, 17, 5, 32, DateTimeKind.Unspecified).AddTicks(2449), "Minnie_Tromp@hotmail.com", "Minnie", "Tromp", new DateTime(2018, 1, 11, 3, 17, 31, 416, DateTimeKind.Unspecified).AddTicks(2298), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1982, 4, 3, 12, 52, 37, 128, DateTimeKind.Unspecified).AddTicks(5016), "Ellen.Hudson@hotmail.com", "Ellen", "Hudson", new DateTime(2016, 4, 26, 5, 10, 5, 597, DateTimeKind.Unspecified).AddTicks(8638), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 12, 26, 18, 2, 13, 530, DateTimeKind.Unspecified).AddTicks(3056), "Ramon.Schmidt50@gmail.com", "Ramon", "Schmidt", new DateTime(2019, 6, 17, 19, 46, 28, 255, DateTimeKind.Unspecified).AddTicks(1838), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 1, 11, 3, 33, 59, 317, DateTimeKind.Unspecified).AddTicks(8120), "Loretta.Gutmann@gmail.com", "Loretta", "Gutmann", new DateTime(2019, 12, 15, 21, 3, 44, 290, DateTimeKind.Unspecified).AddTicks(2289), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1975, 5, 1, 9, 17, 30, 29, DateTimeKind.Unspecified).AddTicks(2830), "Winston_Tromp83@gmail.com", "Winston", "Tromp", new DateTime(2016, 6, 20, 23, 40, 44, 882, DateTimeKind.Unspecified).AddTicks(7485), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 8, 14, 18, 2, 29, 848, DateTimeKind.Unspecified).AddTicks(2620), "Catherine.Stiedemann@hotmail.com", "Catherine", "Stiedemann", new DateTime(2016, 7, 7, 6, 47, 50, 458, DateTimeKind.Unspecified).AddTicks(8736), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 10, 14, 12, 52, 46, 518, DateTimeKind.Unspecified).AddTicks(2716), "Rhonda.Schneider87@hotmail.com", "Rhonda", "Schneider", new DateTime(2018, 11, 4, 16, 22, 7, 643, DateTimeKind.Unspecified).AddTicks(5873), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1970, 12, 20, 1, 35, 47, 670, DateTimeKind.Unspecified).AddTicks(541), "Yvonne.Erdman21@gmail.com", "Yvonne", "Erdman", new DateTime(2016, 1, 18, 7, 38, 32, 88, DateTimeKind.Unspecified).AddTicks(2431), 1 });
        }
    }
}
