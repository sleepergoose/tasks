﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Entities
{
    public sealed class Team
    {
        public Team()
        {
            Members = new List<User>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public ICollection<User> Members { get; set; }
    }
}
