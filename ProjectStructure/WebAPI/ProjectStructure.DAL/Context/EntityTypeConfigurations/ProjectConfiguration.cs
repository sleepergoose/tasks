﻿using ProjectStructure.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProjectStructure.DAL.Context.EntityTypeConfigurations
{
    public sealed class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.Property(p => p.Name)
                 .IsRequired()
                 .HasColumnType("nvarchar(128)")
                 .HasColumnName("ProjectName");


            builder.Property(p => p.Description)
                .IsRequired(false)
                .HasColumnType("nvarchar(512)");


            builder.Property(p => p.CreatedAt)
                .IsRequired()
                .HasColumnType("datetime2(2)")
                .HasDefaultValueSql("GETDATE()");


            builder.Property(p => p.Deadline)
                .IsRequired()
                .HasColumnType("datetime2(2)");


            builder.HasOne(p => p.Team)
                .WithMany()
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.Restrict);


            builder.HasOne(p => p.Author)
                .WithMany()
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
