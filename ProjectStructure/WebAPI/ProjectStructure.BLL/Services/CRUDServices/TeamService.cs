﻿using System;
using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.BLL.Services.Interfaces;

namespace ProjectStructure.BLL.Services
{
    public sealed class TeamService : BaseService, IService<TeamDTO>
    {
        public TeamService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<TeamDTO>> GetAllEntitiesAsync()
        {
            var teams = await _context.Teams.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }


        public async Task<TeamDTO> GetEntityAsync(int id)
        {
            var team = await _context.Teams.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<TeamDTO>(team);
        }


        public async Task<TeamDTO> AddEntityAsync(TeamDTO teamDTO)
        {
            if (teamDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                teamDTO.Id = 0;

                var team = _mapper.Map<DAL.Entities.Team>(teamDTO);

                await _context.Teams.AddAsync(team);

                await _context.SaveChangesAsync();

                var createdTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == team.Id);

                return _mapper.Map<TeamDTO>(createdTeam);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<TeamDTO> UpdateEntityAsync(TeamDTO teamDTO)
        {
            if (teamDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                var team = _mapper.Map<DAL.Entities.Team>(teamDTO);

                _context.Teams.Update(team);

                await _context.SaveChangesAsync();

                var updatedTeam = await _context.Teams.FirstOrDefaultAsync(t => t.Id == team.Id);

                return _mapper.Map<TeamDTO>(updatedTeam);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<int> DeleteEntityAsync(int id)
        {
            try
            {
                var deletedTeam = await _context.Teams.FirstOrDefaultAsync(p => p.Id == id);

                _context.Teams.Remove(deletedTeam);

                await _context.SaveChangesAsync();

                return id;
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
