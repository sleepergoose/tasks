﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.DTO
{
    public sealed class UserDTO
    {
        public int Id { get; set; }

        [Range(0, int.MaxValue)]
        public int? TeamId { get; set; }

        [Required]
        [MinLength(3)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(3)]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public DateTime RegisteredAt { get; set; }

        [Required]
        public DateTime BirthDay { get; set; }
    }
}
