﻿using System;
using System.Collections.Generic;

namespace Client.Common.Entities
{
    public sealed class Project
    {
        public Project()
        {
            Tasks = new List<Task>();
        }

        public int Id { get; set; }

        public int AuthorId { get; set; }
        public User Author { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public DateTime CreatedAt { get; set; }

        public ICollection<Task> Tasks { get; set; }
    }
}

