﻿using System;
using System.Collections.Generic;


namespace Client.Common.DTO
{
    public sealed class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
