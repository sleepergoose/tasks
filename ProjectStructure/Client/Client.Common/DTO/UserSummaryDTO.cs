﻿
namespace Client.Common.DTO
{
    public sealed class UserSummaryDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksAmount { get; set; }
        public int BadTasksAmount { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
