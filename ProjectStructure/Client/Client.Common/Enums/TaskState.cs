﻿namespace Client.Common.Enums
{
    public enum TaskState
    {
        Pending = 0,
        Canceled,
        Unfinished,
        Finished
    }
}
