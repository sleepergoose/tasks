﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Models = Client.Common.Entities;

namespace Client.Interfaces
{
    public interface IQueries
    {
        Task<List<(int Id, string Name)>> GetFinishedTasksAsync(int performerId);

        Task<List<Models.ProjectSummary>> GetProjectSummaryAsync();

        Task<Dictionary<Models.Project, int>> GetTasksAmountAsync(int authorId);

        Task<List<Client.Common.Entities.Task>> GetTasksListAsync(int performerId);

        Task<List<(int Id, string TeamName, List<Models.User> Users)>> GetTeamsMembersAsync();

        Task<Models.UserSummary> GetUserSummaryAsync(int userId);

        Task<List<Models.User>> GetUsersWithTasksAsync();
    }
}
