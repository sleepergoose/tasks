﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Models = Client.Common.Entities;

namespace Client.Interfaces
{
    public interface IView
    {
        Task ShowMenuAsync();

        Task WriteTextToCenter(string text);

        Task ShowTaskAmountAsync(Dictionary<Models.Project, int> dict);

        Task ShowTaskListAsync(List<Client.Common.Entities.Task> tasks);

        Task ShowFinishedTasksAsync(List<(int Id, string Name)> list);

        Task ShowTeamsMembersAsync(List<(int Id, string TeamName, List<Models.User> Users)> teams);

        Task ShowUsersWithTasksAsync(List<Models.User> users);

        Task ShowUserSummaryAsync(Models.UserSummary userSummary);

        Task ShowProjectSummaryAsync(List<Models.ProjectSummary> projectSummaries);

        Task WriteErrorToCenter(string error);

        string GetTextResponse(string message);
    }
}
