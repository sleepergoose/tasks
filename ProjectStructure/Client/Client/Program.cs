﻿using System;
using Client.Data;
using System.Net.Http;
using Client.Interfaces;
using Client.BLL.Services;
using System.ComponentModel;
using System.Threading.Tasks;
using Client.BLL.Services.Interfaces;
using Models = Client.Common.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Client
{
    class Program
    {
        private static ServiceProvider _services;

        private static IConfiguration AppConfiguration { get; set; }


        static async Task Main(string[] args)
        {
            // Settings and IoC
            Configure();


            /* UI */
            IView view = _services.GetService<IView>();
            IQueries queries = _services.GetService<IQueries>();
            QueriesService queriesService = _services.GetService<QueriesService>();

            Presenter presenter = new Presenter(view, queries, queriesService);
            await presenter.Run();


            /* The End */
            Console.ReadLine();

            queriesService.Dispose();
        }



        private static void Configure()
        {
            // Getting server host address from appsettings.json
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            AppConfiguration = builder.Build();

            var host = AppConfiguration.GetSection("host").Value;


            // IoC
            _services = new ServiceCollection()
                .AddScoped<HttpClient>()
                .AddScoped<HttpService>()
                .AddScoped<BackgroundWorker>()

                .AddScoped<IService<Models.Project>>(_ => new ProjectService(host, _services.GetService<HttpService>()))
                .AddScoped<IService<Models.User>>(_ => new UserService(host, _services.GetService<HttpService>()))
                .AddScoped<IService<Models.Team>>(_ => new TeamService(host, _services.GetService<HttpService>()))
                .AddScoped<IService<Models.Task>>(_ => new TaskService(host, _services.GetService<HttpService>()))
                .AddScoped(_ => new LinqService(host, _services.GetService<HttpService>()))

                .AddSingleton<ITimerService, TimerService>()
                .AddSingleton<QueriesService>()

                //.AddScoped<IDataLoader, DataLoader>() // Uncomment with the next line
                //.AddScoped<IQueries, LocalQueries>()  // Local queries as in the first task  
                .AddScoped<IQueries, RemoteQueries>()   // Queries to the remote server

                .AddSingleton<IView, View>()
                .BuildServiceProvider();
        }
    }
}
