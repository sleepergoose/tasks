﻿using Client.Interfaces;
using Client.BLL.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using Models = Client.Common.Entities;


namespace Client.Data
{
    public sealed class RemoteQueries : IQueries
    {

        private readonly LinqService _linqService;

        public RemoteQueries(LinqService linqService)
        {
            _linqService = linqService;
        }


        /* -- 1 -- */
        public async Task<Dictionary<Models.Project, int>> GetTasksAmountAsync(int authorId)
        {
            return await _linqService.GetTasksAmountAsync(authorId);
        }


        /* -- 2 -- */
        public async Task<List<Models.Task>> GetTasksListAsync(int performerId)
        {
            return await _linqService.GetTasksListAsync(performerId);
        }


        /* -- 3 -- */
        public async Task<List<(int Id, string Name)>> GetFinishedTasksAsync(int performerId)
        {
            return await _linqService.GetFinishedTasksAsync(performerId);
        }


        /* -- 4 -- */
        public async Task<List<(int Id, string TeamName, List<Models.User> Users)>> GetTeamsMembersAsync()
        {
            return await _linqService.GetTeamsMembersAsync();
        }


        /* -- 5 -- */
        public async Task<List<Models.User>> GetUsersWithTasksAsync()
        {
            return await _linqService.GetUsersWithTasksAsync();
        }


        /* -- 6 -- */
        public async Task<Models.UserSummary> GetUserSummaryAsync(int userId)
        {
            return await _linqService.GetUserSummaryAsync(userId);
        }


        /* -- 7 -- */
        public async Task<List<Models.ProjectSummary>> GetProjectSummaryAsync()
        {
            return await _linqService.GetProjectSummaryAsync();
        }
    }
}
