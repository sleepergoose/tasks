﻿using System;
using System.Linq;
using Nito.AsyncEx;
using Client.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using Models = Client.Common.Entities;

namespace Client.Data
{
    public sealed class LocalQueries : IQueries
    {
        private List<Models.Project> _projects;
        private readonly IDataLoader _dataLoader;

        private AsyncLock _asyncLock = new AsyncLock();

        public LocalQueries(IDataLoader dataLoader)
        {
            _dataLoader = dataLoader;
        }

        private async Task SetData()
        {
            using (await _asyncLock.LockAsync())

            if (_projects == null)
                _projects = await _dataLoader.GetDataStructure();
        }


        /* -- 7 -- */
        public async Task<List<Models.ProjectSummary>> GetProjectSummaryAsync()
        {
            await SetData();

            return await Task.Run(() => _projects.Select(project =>
                 new Models.ProjectSummary
                 {
                     Project = project,
                     LongestTaskByDescription = project.Tasks?.OrderByDescending(task => task.Description.Length).FirstOrDefault(),
                     ShortestTaskByName = project.Tasks?.OrderBy(task => task.Name.Length).FirstOrDefault(),
                     TeamMemberAmount = (project.Description.Length > 20 || (project.Tasks == null ? 0 : project.Tasks.Count()) < 3)
                                        ? (project.Team == null ? 0 :
                                            _projects.Where(p => p.Tasks != null)
                                                .SelectMany(p => p.Tasks.Select(t => t.Performer))
                                                .Distinct()
                                                .Where(u => u.TeamId == project.TeamId)
                                                .Count()) : 0
                 }).ToList());
        }


        /* -- 6 -- */
        public async Task<Models.UserSummary> GetUserSummaryAsync(int userId)
        {
            await SetData();

            return await Task.Run(() => _projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks.Select(task => task.Performer))
                .Distinct()
                .Where(user => user.Id == userId)
                .Select(user =>
                {

                    var lastUserProject = _projects
                        .Where(p => p.AuthorId == userId)
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault();

                    return new Models.UserSummary
                    {
                        User = user,
                        LastProject = lastUserProject,
                        LastProjectTasksAmount = lastUserProject?.Tasks == null ? 0 : lastUserProject.Tasks.Count(),

                        BadTasksAmount = _projects.Where(project => project.Tasks != null)
                                            .SelectMany(project => project.Tasks)
                                            .Where(task => task.PerformerId == user.Id && task.FinishedAt == null)
                                            .Count(),

                        LongestTask = _projects.Where(project => project.Tasks != null)
                                            .SelectMany(project => project.Tasks)
                                            .Where(task => task.PerformerId == user.Id)
                                            .OrderByDescending(t => (t.FinishedAt == null ? DateTime.Now : t.FinishedAt) - t.CreatedAt)
                                            .FirstOrDefault()
                    };
                }).FirstOrDefault());
        }


        /* -- 5 -- */
        public async Task<List<Models.User>> GetUsersWithTasksAsync()
        {
            await SetData();

            return await Task.Run(() => _projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks.Select(task => task.Performer))
                .Distinct()
                .OrderBy(performer => performer.FirstName)
                .Select(performer => new Models.User
                {
                    Id = performer.Id,
                    FirstName = performer.FirstName,
                    BirthDay = performer.BirthDay,
                    Email = performer.Email,
                    LastName = performer.LastName,
                    RegisteredAt = performer.RegisteredAt,
                    Team = performer.Team,
                    TeamId = performer.TeamId,
                    Tasks = _projects.Where(project => project.Tasks != null)
                        .SelectMany(p => p.Tasks)
                        .Where(task => task.PerformerId == performer.Id)
                        .OrderByDescending(user => user.Name.Length)
                        .ToList()
                }).ToList());
        }


        /* -- 4 -- */
        public async Task<List<(int Id, string TeamName, List<Models.User> Users)>> GetTeamsMembersAsync()
        {
            await SetData();

            return await Task.Run(() => _projects.Select(p => p.Team).Where(team =>
            {
                return !_projects.Where(p => p.Tasks != null)
                     .SelectMany(p => p.Tasks.Select(task => task.Performer))
                     .Where(perfomer => perfomer.BirthDay.Year > DateTime.Now.Year - 10)
                     .Any(perfomer => perfomer.TeamId == team.Id);
            })
               .Distinct()
               .Select(team =>
                   (
                       Id: team.Id,
                       TeamName: team.Name,
                       Users: _projects.Where(p => p.Tasks != null)
                                   .SelectMany(p => p.Tasks.Select(task => task.Performer))
                                   .Distinct()
                                   .Where(performer => performer.TeamId == team.Id)
                                   .OrderByDescending(performer => performer.RegisteredAt).ToList()
                   )).ToList());
        }


        /* -- 3 -- */
        public async Task<List<(int Id, string Name)>> GetFinishedTasksAsync(int performerId)
        {
            await SetData();

            return await Task.Run(() => _projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks)
                .Where(task =>
                        task.PerformerId == performerId &&
                        task.FinishedAt?.Year == DateTime.Now.Year)
                .Select(task => (id: task.Id, name: task.Name))
                .ToList());
        }


        /* -- 2 -- */
        public async Task<List<Client.Common.Entities.Task>> GetTasksListAsync(int performerId)
        {
            await SetData();

            return await Task.Run(() => _projects.Where(project => project.Tasks != null)
                .SelectMany(project => project.Tasks)
                .Where(task => task.PerformerId == performerId && task.Name.Length < 45)
                .ToList());
        }


        /* -- 1 -- */
        public async Task<Dictionary<Models.Project, int>> GetTasksAmountAsync(int authorId)
        {
            await SetData();

            return await Task.Run(() => _projects.Where(project => project.AuthorId == authorId)
                .ToDictionary(key => key, value => (value.Tasks == null ? 0 : value.Tasks.Count())));
        }
    }
}
