﻿using System.Linq;
using Client.Interfaces;
using Client.BLL.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services.Interfaces;
using Models = Client.Common.Entities;

namespace Client
{
    public class DataLoader : IDataLoader
    {
        private readonly IService<Models.Project> _projectService;
        private readonly IService<Models.Task> _taskService;
        private readonly IService<Models.Team> _teamService;
        private readonly IService<Models.User> _userService;


        public DataLoader(IService<Models.Project> projectService, IService<Models.Task> taskService,
                          IService<Models.Team> teamService, IService<Models.User> userService)
        {
            _projectService = projectService;
            _taskService = taskService;
            _teamService = teamService;
            _userService = userService;
        }


        public async Task<List<Models.Project>> GetDataStructure()
        {
            try
            {
                var tasksList = new List<Task>()
                {
                    Task.Run(() => _projectService.GetAllEntitiesAsync()),
                    Task.Run(() => _teamService.GetAllEntitiesAsync()),
                    Task.Run(() => _userService.GetAllEntitiesAsync()),
                    Task.Run(() => _taskService.GetAllEntitiesAsync()),
                };

                await Task.WhenAll(tasksList);

                // All the requests have been already executed above in Task.WhenAll
                // Get results of the requests here 
                var projects = ((Task<IEnumerable<Models.Project>>)tasksList[0]).Result;
                var teams = ((Task<IEnumerable<Models.Team>>)tasksList[1]).Result;
                var users = ((Task<IEnumerable<Models.User>>)tasksList[2]).Result;
                var tasks = ((Task<IEnumerable<Models.Task>>)tasksList[3]).Result; 



                await Task.Run(() =>
                    projects = projects.GroupJoin(
                    tasks.Join(users, task => task.PerformerId, user => user.Id, (task, user) =>
                    {
                        task.Performer = user;
                        return task;
                    }),
                    project => project.Id, task => task.ProjectId, (project, _tasks) =>
                    {
                        project.Tasks = _tasks as ICollection<Client.Common.Entities.Task>;
                        return project;
                    })
                    .Join(users, project => project.AuthorId, user => user.Id, (project, user) =>
                    {
                        project.Author = user;
                        return project;
                    })
                    .Join(teams, project => project.TeamId, team => team.Id, (project, team) =>
                    {
                        project.Team = team;
                        return project;
                    })
                );

                return projects.ToList();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine($"Error: {ex.Message}");
                throw new System.Exception();
            }
        }
    }
}
