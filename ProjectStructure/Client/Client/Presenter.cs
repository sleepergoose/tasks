﻿using Client.Interfaces;
using Client.BLL.Services;
using System.Threading.Tasks;

namespace Client
{
    public sealed class Presenter
    {
        private readonly IView _view;
        private readonly IQueries _queries;
        private readonly QueriesService _queriesService;

        private bool _exitFlag;

        public Presenter(IView view, IQueries queries, QueriesService queriesService)
        {
            _view = view;
            _queries = queries;
            _exitFlag = false;
            _queriesService = queriesService;
        }


        public async Task Run()
        {
            await _view.ShowMenuAsync();
            await MainManager(_view.GetTextResponse("Enter method number or exit"));
        }

        
        private async Task MainManager(string command)
        {
            await _view.ShowMenuAsync();

            try
            {
                switch (command)
                {
                    case "1":
                            int authorId;
                            if (int.TryParse(_view.GetTextResponse("Enter AuthorId (for example: 13)"), out authorId) == true)
                            {
                                var query1 = await _queries.GetTasksAmountAsync(authorId);
                                await _view.ShowTaskAmountAsync(query1);
                            }
                            else
                            {
                                await _view.WriteErrorToCenter("You entered wrong parameter");
                            }
                        break;

                    case "2":
                            int performerId;
                            if (int.TryParse(_view.GetTextResponse("Enter PerformerId (for example: 3)"), out performerId) == true)
                            {
                                var query2 = await _queries.GetTasksListAsync(performerId);
                                await _view.ShowTaskListAsync(query2);
                            }
                            else
                            {
                                await _view.WriteErrorToCenter("You entered wrong parameter");
                            }
                        break;

                    case "3":
                            if (int.TryParse(_view.GetTextResponse("Enter PerformerId (for example: 5)"), out performerId) == true)
                            {
                                var query3 = await _queries.GetFinishedTasksAsync(performerId);
                                await _view.ShowFinishedTasksAsync(query3);
                            }
                            else
                            {
                                await _view.WriteErrorToCenter("You entered wrong parameter");
                            }
                        break;

                    case "4":
                            var query4 = await _queries.GetTeamsMembersAsync();
                            await _view.ShowTeamsMembersAsync(query4);
                        break;
                    case "5":
                            var query5 = await _queries.GetUsersWithTasksAsync();
                            await _view.ShowUsersWithTasksAsync(query5);
                        break;

                    case "6":
                            int userId;
                            if (int.TryParse(_view.GetTextResponse("Enter PerformerId (for example: 13)"), out userId) == true)
                            {
                                var query6 = await _queries.GetUserSummaryAsync(userId: userId);
                                await _view.ShowUserSummaryAsync(query6);
                            }
                            else
                            {
                                await _view.WriteErrorToCenter("You entered wrong parameter");
                            }
                        break;

                    case "7":
                            var query7 = await _queries.GetProjectSummaryAsync();
                            await _view.ShowProjectSummaryAsync(query7);
                        break;

                    case "8":
                        int delay = 0;
                        if (int.TryParse(_view.GetTextResponse("Enter delay in ms (more than or equal 1000)"), out delay) == true)
                        {
                            delay = delay < 1000 ? 1000 : delay;

                            var markedTaskId = await _queriesService.MarkRandomTaskWithDelay(delay);
                            await _view.WriteTextToCenter($"Updated task Id in background: {markedTaskId}");
                            await _view.WriteTextToCenter($"Started background process is still running");
                        }
                        else
                        {
                            await _view.WriteErrorToCenter("You entered wrong parameter");
                        }
                        break;

                    case "exit":
                            await _view.WriteTextToCenter("Good Luck!");
                            _view.GetTextResponse("...Press 'Enter' key to exit...");
                            _exitFlag = true;
                        break;
                }
            }
            catch (System.Exception ex)
            {
                await _view.WriteErrorToCenter(ex.Message);
            }
            finally
            {
                if(!_exitFlag)
                    await MainManager(_view.GetTextResponse("Enter method number or exit"));
            }
        }
    }
}
