﻿using System;
using System.Linq;
using Client.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using Models = Client.Common.Entities;

namespace Client
{
    public sealed class View : IView
    {
        private readonly List<string> _menu = new List<string>()
        {
            "1. GetTasksAmount(authorId: int)",
            "2. GetTasksList(performerId: int)",
            "3. GetFinishedTasks(performerId: int)",
            "4. GetTeamsMembers()",
            "5. GetUsersWithTasks()",
            "6. GetUserSummary(userId: int)",
            "7. GetProjectSummary()",
            "8. Mark Random Task With Delay"
        };


        public async Task ShowProjectSummaryAsync(List<Models.ProjectSummary> projectSummaries)
        {
            await Task.Run(() => {
                Console.WriteLine("");
                Console.WriteLine("View only for the firts item");
                Console.WriteLine("\t{0,-20} | {1, -20} | {2, -20} | {3, -35} |", "Project", "Longest Task", "Shortest Tasks", "Performers Amount");
                Console.WriteLine("\t-----------------------------------------------------------------------------------------------------------");

                var first = projectSummaries.FirstOrDefault();

                Console.WriteLine("\t{0,-20} | {1, -20} | {2, -20} | {3, -35} |",
                    first.Project.Name.Substring(0, GetIndex(first.Project.Name, 20)),
                    first.LongestTaskByDescription?.Description.Substring(0, GetIndex(first.LongestTaskByDescription.Description, 20)),
                    first.ShortestTaskByName?.Name.Substring(0, GetIndex(first.ShortestTaskByName.Name, 20)),
                    first.TeamMemberAmount);
                Console.WriteLine("");
            });
        }


        public async Task ShowUserSummaryAsync(Models.UserSummary userSummary)
        {
            await Task.Run(() => {
                Console.WriteLine("");
                if (userSummary != null)
                {
                    Console.WriteLine("\t{0,-10} | {1, -20} | {2, -15} | {3, -10} | {4, -35} |", "User", "Last Project", "Project Tasks", "Bad Tasks", "Longest Task");
                    Console.WriteLine("\t----------------------------------------------------------------------------------------------------------");

                    Console.WriteLine("\t{0,-10} | {1, -20} | {2, -15} | {3, -10} | {4, -35} |",
                        userSummary.User.FirstName, userSummary.LastProject?.Name.Substring(0, GetIndex(userSummary.LastProject?.Name, 20)),
                        userSummary.LastProjectTasksAmount, userSummary.BadTasksAmount,
                        $"{userSummary.LongestTask?.Name} ({userSummary.LongestTask?.CreatedAt.ToShortDateString()} - " +
                        $"{userSummary.LongestTask?.FinishedAt?.ToShortDateString() ?? DateTime.Now.ToShortDateString()})");
                }
                else
                {
                    Console.WriteLine("\tThere is no such a user");
                }
                Console.WriteLine("");
            });
        }


        public async Task ShowUsersWithTasksAsync(List<Models.User> users)
        {
            await Task.Run(() => {
                Console.WriteLine("");
                Console.WriteLine(string.Join(", ", users.Select(u => u.FirstName)));
                Console.WriteLine("");
                Console.WriteLine("\tОтсортированный по длине name (по убыванию) список тасков первого пользователя:");

                foreach (var item in users.FirstOrDefault().Tasks)
                {
                    Console.WriteLine($"\t\t{item.Name}");
                }
                Console.WriteLine("");
            });
        }


        public async Task ShowTeamsMembersAsync(List<(int Id, string TeamName, List<Models.User> Users)> teams)
        {
            await Task.Run(() => {
                Console.WriteLine("");
                Console.WriteLine("\t{0,-5} | {1, -25} | {2, -50} |", "ID", "Task Name", "Teams List");
                Console.WriteLine("\t-------------------------------------------------------------------------------------------------------");

                foreach (var item in teams)
                {
                    Console.WriteLine("\t{0, -5} | {1, -25} | {2, -65} |", item.Id, item.TeamName.Substring(0, GetIndex(item.TeamName, 20)),
                        string.Join(", ", item.Users.Select(u => u.FirstName))
                            .Substring(0, GetIndex(string.Join(", ", item.Users.Select(u => u.FirstName)), 60)) + "...");
                }
                Console.WriteLine("");
            });
        }


        public async Task ShowFinishedTasksAsync(List<(int Id, string Name)> list)
        {
            await Task.Run(() => {
                Console.WriteLine("");
                if (list != null && list.Count() != 0)
                {
                    Console.WriteLine("\t{0,-5} | {1, -25} |", "ID", "Task Name");
                    Console.WriteLine("\t-----------------------------------");
                    foreach (var task in list)
                    {
                        Console.WriteLine("\t{0, -5} | {1, -25} |", task.Id, task.Name.Substring(0, GetIndex(task.Name, 20)));
                    }
                }
                else
                {
                    Console.WriteLine("\tThe user has no tasks");
                }
                Console.WriteLine("");
            });
        }

        public async Task ShowTaskListAsync(List<Models.Task> tasks)
        {
            await Task.Run(() => {
                Console.WriteLine("");
                if (tasks != null && tasks.Count() != 0)
                {
                    Console.WriteLine("\t{0,-5} | {1, -25} | {2, -50} |", "ID", "Task Name", "Description");
                    Console.WriteLine("\t----------------------------------------------------------------------------------------");
                    foreach (var task in tasks)
                    {
                        Console.WriteLine("\t{0, -5} | {1, -25} | {2, -50} |", task.Id, task.Name.Substring(0, GetIndex(task.Name, 20)),
                            task.Description.Substring(0, GetIndex(task.Description, 40)) + "...");
                    }
                }
                else
                {
                    Console.WriteLine("\tThe user has no tasks");
                }
                Console.WriteLine("");
            });
        }


        public async Task ShowTaskAmountAsync(Dictionary<Models.Project, int> dict)
        {
            await Task.Run(() => {
                Console.WriteLine("");
                if (dict != null && dict.Count() != 0)
                {
                    Console.WriteLine("\t{0,-3} | {1, -45} | {2, -14} |", "ID", "Project Name", "Tasks Amount");
                    Console.WriteLine("\t----------------------------------------------------------------------");

                    foreach (var kvp in dict)
                    {
                        Console.WriteLine("\t{0,-3} | {1, -45} | {2, -14} |", kvp.Key.Id, kvp.Key.Name.Substring(0, GetIndex(kvp.Key.Name, 40)), kvp.Value);
                    }
                }
                else
                {
                    Console.WriteLine("\tThe user has no projects");
                }
                Console.WriteLine("");
            });
        }


        public async Task ShowMenuAsync()
        {
            await Task.Run(async ()=> {
                Console.Clear();
                
                int width = Console.WindowWidth;

                // Write titles
                Console.Title = "Project Structure - Client";
                await WriteTextToCenter("Select menu item below:");
                Console.WriteLine(string.Concat(Enumerable.Repeat("-", width)));

                foreach (var item in _menu)
                {
                    Console.WriteLine($"\t{item}");
                }
                Console.WriteLine(string.Concat(Enumerable.Repeat("-", width)));
            });
        }

        public async Task WriteErrorToCenter(string text)
        {
            await Task.Run(() => {
                // Some required variables
                int left = (Console.WindowWidth - text.Length) / 2;

                // Set cursor
                Console.WriteLine("");
                (_, int top) = Console.GetCursorPosition();
                Console.SetCursorPosition(left, top);

                // Draw
                ColorWrite($"{text}", ConsoleColor.Red);
                Console.WriteLine("");
            });
        }

        // I think, this method is executed synchronously (it bocks thread because of 'Console.ReadLine')
        // So I didn't remake this method to async 
        public string GetTextResponse(string message)
        {
            Console.WriteLine($"{message}:");
            return Console.ReadLine();
        }


        public async Task WriteTextToCenter(string text)
        {
            await Task.Run(() => {
                // Some required variables
                int left = (Console.WindowWidth - text.Length) / 2;

                // Set cursor
                Console.WriteLine("");
                (_, int top) = Console.GetCursorPosition();
                Console.SetCursorPosition(left, top);

                // Draw
                ColorWrite($"{text}", ConsoleColor.Yellow);
                Console.WriteLine("");
            });
        }


        private void ColorWrite(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = ConsoleColor.White;
        }


        private static int GetIndex(string text, int limit) =>
                text.Length > limit ? limit : text.Length;

    }
}
