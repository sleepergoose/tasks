﻿using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.BLL.Services
{
    public sealed class HttpService
    {
        private readonly HttpClient _httpClient;

        public HttpService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }


        public async Task<string> GetStringAsync(string url)
        {
            return await _httpClient.GetStringAsync(url);
        }


        public async Task<HttpResponseMessage> AddEntityAsync(string url, string contentInJson)
        {
            return await _httpClient.PostAsync(url, new StringContent(contentInJson, Encoding.UTF8, "application/json"));
        }


        public async Task<HttpResponseMessage> UpdateEntityAsync(string url, string contentInJson)
        {
            return await _httpClient.PutAsync(url, new StringContent(contentInJson, Encoding.UTF8, "application/json"));
        }


        public async Task<HttpResponseMessage> DeleteEntityAsync(string url, int entityId)
        {
            return await _httpClient.DeleteAsync($"{url}/{entityId}");
        }
    }
}
