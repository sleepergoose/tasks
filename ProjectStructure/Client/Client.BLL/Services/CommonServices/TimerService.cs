﻿using System;
using System.Timers;
using Client.BLL.Services.Interfaces;

namespace Client.BLL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;
        private double _interval;

        public double Interval
        {
            get
            {
                return _interval;
            }
            set
            {
                if (value > 0 && value < int.MaxValue)
                    _interval = value;
                else
                    throw new Exception("Wrong value of the property Interval");
            }
        }

        public event ElapsedEventHandler Elapsed;

        public TimerService()
        {
            _timer = new Timer();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer.Interval = _interval;

            _timer.Elapsed += this.Elapsed;

            _timer.AutoReset = true;

            // Start the timer
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Elapsed -= this.Elapsed;
            _timer.Stop();
        }
    }
}