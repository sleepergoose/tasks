﻿using System.Linq;
using Newtonsoft.Json;
using Client.Common.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services.Abstract;
using Models = Client.Common.Entities;

namespace Client.BLL.Services
{
    public class LinqService : BaseService
    {
        private readonly string _url;

        public LinqService(string host, HttpService httpService) : base(host, httpService)
        {
            _url = $"{Host}/api/Linq";
        }


        /* -- 1 -- */
        public async Task<Dictionary<Models.Project, int>> GetTasksAmountAsync(int authorId)
        {
            var response = await _httpService.GetStringAsync($"{_url}/TasksAmount/{authorId}");

            var list = JsonConvert.DeserializeObject<List<KeyValuePair<ProjectDTO, int>>>(response);

            var dict = list.ToDictionary(x => x.Key, x => x.Value);

            return _mapper.Map<Dictionary<Models.Project, int>>(dict); ;
        }


        /* -- 2 -- */
        public async Task<List<Models.Task>> GetTasksListAsync(int performerId)
        {
            var response = await _httpService.GetStringAsync($"{_url}/TasksList/{performerId}");

            var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(response);

            return _mapper.Map<List<Models.Task>>(tasks); ;
        }


        /* -- 3 -- */
        public async Task<List<(int Id, string Name)>> GetFinishedTasksAsync(int performerId)
        {
            var response = await _httpService.GetStringAsync($"{_url}/FinishedTasks/{performerId}");

            var tasks = JsonConvert.DeserializeObject<List<(int Id, string Name)>>(response);

            return tasks;
        }


        /* -- 4 -- */
        public async Task<List<(int Id, string TeamName, List<Models.User> Users)>> GetTeamsMembersAsync()
        {
            var response = await _httpService.GetStringAsync($"{_url}/TeamsMembers");

            var tasks = JsonConvert.DeserializeObject<List<(int Id, string TeamName, List<UserDTO> Users)>>(response);

            return tasks.Select(task => 
                    (Id: task.Id, TeamName: task.TeamName, Users: _mapper.Map<List<Models.User>>(task.Users)))
                .ToList();
        }


        /* -- 5 -- */
        public async Task<List<Models.User>> GetUsersWithTasksAsync()
        {
            var response = await _httpService.GetStringAsync($"{_url}/UsersWithTasks");

            var users = JsonConvert.DeserializeObject<List<UserWithTasksDTO>>(response);

            return await Task.Run(() => _mapper.Map<List<Models.User>>(users.Select(u => u.User))
                    .GroupJoin(users.SelectMany(u => u.Tasks), user => user.Id, task => task.PerformerId,
                        (user, _tasks) => {
                            user.Tasks = _mapper.Map<List<Common.Entities.Task>>(_tasks);
                            return user;
                        })
                    .ToList());
        }


        /* -- 6 -- */
        public async Task<Models.UserSummary> GetUserSummaryAsync(int userId)
        {
            var response = await _httpService.GetStringAsync($"{_url}/UserSummary/{userId}");
            
            var user = JsonConvert.DeserializeObject<UserSummaryDTO>(response);

            return _mapper.Map<Models.UserSummary>(user);
        }


        /* -- 7 -- */
        public async Task<List<Models.ProjectSummary>> GetProjectSummaryAsync()
        {
            var response = await _httpService.GetStringAsync($"{_url}/ProjectSummary");

            var projects = JsonConvert.DeserializeObject<List<ProjectSummaryDTO>>(response);

            return _mapper.Map<List<Models.ProjectSummary>>(projects);
        }
    }
}
