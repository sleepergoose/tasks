﻿using System;
using System.Linq;
using System.Timers;
using Client.Common.Enums;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services.Interfaces;
using Models = Client.Common.Entities;

namespace Client.BLL.Services
{
    public class QueriesService : IDisposable
    {
        private readonly ITimerService _timerService;
        private readonly BackgroundWorker _bgWorker;
        private readonly IService<Models.Task> _taskService;

        private bool disposedValue;


        public QueriesService(ITimerService timerService, IService<Models.Task> taskService, BackgroundWorker bgWorker)
        {
            _timerService = timerService;
            _taskService = taskService;
            _bgWorker = bgWorker;

            _timerService.Elapsed += StartBackgroundWorker;
        }


        public Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tcs = new TaskCompletionSource<int>();

            _timerService.Interval = delay;
            _timerService.Start();


            _bgWorker.DoWork += async (o, e) =>
            {
                try
                {
                    _timerService.Stop();
                    var tasks = await GetUnfinishedTaskListAsync();
                    var task = await GetRandomTaskAsync(tasks);
                    var updatedTask = await SetTaskAsFinishedAsync(task);

                    if (updatedTask != null)
                    {
                        _timerService.Start();
                        tcs.TrySetResult(updatedTask.Id);
                    }
                }
                catch (Exception ex)
                {
                    tcs.TrySetException(new Exception(ex.Message));
                }
            };

            return tcs.Task;
        }

        private async Task<Models.Task> SetTaskAsFinishedAsync(Models.Task task)
        {
            task.FinishedAt = DateTime.Now;
            task.State = TaskState.Finished;

            // return await Task.Run(() => task); // This line is for smoke testing not to update all tasks in database
            return await _taskService.UpdateEntityAsync(task);
        }


        private async Task<Models.Task> GetRandomTaskAsync(IEnumerable<Models.Task> tasks)
        {
            return await Task.Run(() => {

                Random rnd = new Random();

                int rndIndex = rnd.Next(1, tasks.Count());

                return tasks.ElementAt(rndIndex);
            });
        }


        private async Task<List<Models.Task>> GetUnfinishedTaskListAsync()
        {
            return (await _taskService.GetAllEntitiesAsync())
                .Where(task => task.FinishedAt == null)
                .ToList();
        }


        private void StartBackgroundWorker(object sender, ElapsedEventArgs e)
        {
            if(!_bgWorker.IsBusy)
                _bgWorker.RunWorkerAsync();
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _timerService.Stop();
                    _timerService.Dispose();
                    _bgWorker.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

