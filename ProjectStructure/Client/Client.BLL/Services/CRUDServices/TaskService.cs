﻿using Newtonsoft.Json;
using Client.Common.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services.Abstract;
using Client.BLL.Services.Interfaces;
using Models = Client.Common.Entities;

namespace Client.BLL.Services
{
    public class TaskService : BaseService, IService<Models.Task>
    {
        private readonly string _url;

        public TaskService(string host, HttpService httpService) : base(host, httpService)
        {
            _url = $"{Host}/api/Tasks";
        }

        public async Task<IEnumerable<Models.Task>> GetAllEntitiesAsync()
        {
            var response = await _httpService.GetStringAsync(_url);

            var tasks = await Task.Run(() => JsonConvert.DeserializeObject<ICollection<TaskDTO>>(response));

            return _mapper.Map<IEnumerable<Models.Task>>(tasks);
        }


        public async Task<Models.Task> GetEntityAsync(int id)
        {
            var response = await _httpService.GetStringAsync($"{_url}/{id}");

            var task = JsonConvert.DeserializeObject<TaskDTO>(response);

            return _mapper.Map<Models.Task>(task);
        }


        public async Task<Models.Task> AddEntityAsync(Models.Task task)
        {
            var taskInJson = JsonConvert.SerializeObject(_mapper.Map<TaskDTO>(task));

            var httpResponse = await _httpService.AddEntityAsync($"{_url}", taskInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var taskDto = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            return _mapper.Map<Models.Task>(taskDto);
        }



        public async Task<Models.Task> UpdateEntityAsync(Models.Task task)
        {
            var taskInJson = JsonConvert.SerializeObject(_mapper.Map<TaskDTO>(task));            

            var httpResponse = await _httpService.UpdateEntityAsync($"{_url}", taskInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var taskDto = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            return _mapper.Map<Models.Task>(taskDto);
        }


        public async Task<int> DeleteEntityAsync(int entityId)
        {
            var httpResponse = await _httpService.DeleteEntityAsync($"{_url}", entityId);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                return int.Parse(stringResponse);
            else
                return 0;
        }
    }
}
