﻿using Newtonsoft.Json;
using Client.Common.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services.Abstract;
using Client.BLL.Services.Interfaces;
using Models = Client.Common.Entities;

namespace Client.BLL.Services
{
    public class TeamService : BaseService, IService<Models.Team>
    {
        private readonly string _url;

        public TeamService(string host, HttpService httpService) : base(host, httpService)
        {
            _url = $"{Host}/api/Teams";
        }


        public async Task<IEnumerable<Models.Team>> GetAllEntitiesAsync()
        {
            var response = await _httpService.GetStringAsync(_url);

            var teams = await Task.Run(() => JsonConvert.DeserializeObject<ICollection<TeamDTO>>(response));

            return _mapper.Map<IEnumerable<Models.Team>>(teams);
        }


        public async Task<Models.Team> GetEntityAsync(int id)
        {
            var response = await _httpService.GetStringAsync($"{_url}/{id}");
            
            var team = JsonConvert.DeserializeObject<TeamDTO>(response);
            
            return _mapper.Map<Models.Team>(team);
        }


        public async Task<Models.Team> AddEntityAsync(Models.Team team)
        {
            var teamInJson = JsonConvert.SerializeObject(_mapper.Map<TeamDTO>(team));

            var httpResponse = await _httpService.AddEntityAsync($"{_url}", teamInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var teamDto = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            return _mapper.Map<Models.Team>(teamDto);
        }


        public async Task<Models.Team> UpdateEntityAsync(Models.Team team)
        {
            var teamInJson = JsonConvert.SerializeObject(_mapper.Map<TeamDTO>(team));

            var httpResponse = await _httpService.UpdateEntityAsync($"{_url}", teamInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var teamDto = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            return _mapper.Map<Models.Team>(teamDto);
        }


        public async Task<int> DeleteEntityAsync(int entityId)
        {
            var httpResponse = await _httpService.DeleteEntityAsync($"{_url}", entityId);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                return int.Parse(stringResponse);
            else
                return 0;
        }
    }
}
