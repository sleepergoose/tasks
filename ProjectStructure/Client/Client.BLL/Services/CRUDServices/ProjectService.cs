﻿using Newtonsoft.Json;
using Client.Common.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services.Abstract;
using Client.BLL.Services.Interfaces;
using Models = Client.Common.Entities;

namespace Client.BLL.Services
{
    public class ProjectService : BaseService, IService<Models.Project>
    {
        private readonly string _url;

        public ProjectService(string host, HttpService httpService) : base(host, httpService)
        {
            _url = $"{Host}/api/Projects";
        }


        public async Task<IEnumerable<Models.Project>> GetAllEntitiesAsync()
        {
            var response = await _httpService.GetStringAsync(_url);

            var projects = await Task.Run(() => JsonConvert.DeserializeObject<ICollection<ProjectDTO>>(response));

            return _mapper.Map<IEnumerable<Models.Project>>(projects);
        }


        public async Task<Models.Project> GetEntityAsync(int id)
        {
            var response = await _httpService.GetStringAsync($"{_url}/{id}");

            var project = JsonConvert.DeserializeObject<ProjectDTO>(response);

            return _mapper.Map<Models.Project>(project);
        }


        public async Task<Models.Project> AddEntityAsync(Models.Project project)
        {
            var projetInJson = JsonConvert.SerializeObject(_mapper.Map<TaskDTO>(project));

            var httpResponse = await _httpService.AddEntityAsync($"{_url}", projetInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var projectDto = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            return _mapper.Map<Models.Project>(projectDto);
        }


        public async Task<Models.Project> UpdateEntityAsync(Models.Project project)
        {
            var projetInJson = JsonConvert.SerializeObject(_mapper.Map<TaskDTO>(project));

            var httpResponse = await _httpService.UpdateEntityAsync($"{_url}", projetInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var projectDto = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            return _mapper.Map<Models.Project>(projectDto);
        }


        public async Task<int> DeleteEntityAsync(int entityId)
        {
            var httpResponse = await _httpService.DeleteEntityAsync($"{_url}", entityId);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                return int.Parse(stringResponse);
            else
                return 0;
        }
    }
}
