﻿using Newtonsoft.Json;
using Client.Common.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services.Abstract;
using Client.BLL.Services.Interfaces;
using Models = Client.Common.Entities;

namespace Client.BLL.Services
{
    public class UserService : BaseService, IService<Models.User>
    {
        private readonly string _url;

        public UserService(string host, HttpService httpService) : base(host, httpService)
        {
            _url = $"{Host}/api/Users";
        }


        public async Task<IEnumerable<Models.User>> GetAllEntitiesAsync()
        {
            var response = await _httpService.GetStringAsync(_url);

            var users = await Task.Run(() => JsonConvert.DeserializeObject<ICollection<UserDTO>>(response));

            return _mapper.Map<IEnumerable<Models.User>>(users);
        }


        public async Task<Models.User> GetEntityAsync(int id)
        {
            var response = await _httpService.GetStringAsync($"{_url}/{id}");

            var user = JsonConvert.DeserializeObject<UserDTO>(response);

            return _mapper.Map<Models.User>(user);
        }


        public async Task<Models.User> AddEntityAsync(Models.User user)
        {
            var userInJson = JsonConvert.SerializeObject(_mapper.Map<UserDTO>(user));

            var httpResponse = await _httpService.AddEntityAsync($"{_url}", userInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var userDto = JsonConvert.DeserializeObject<UserDTO>(stringResponse);

            return _mapper.Map<Models.User>(userDto);
        }


        public async Task<Models.User> UpdateEntityAsync(Models.User user)
        {
            var userInJson = JsonConvert.SerializeObject(_mapper.Map<UserDTO>(user));

            var httpResponse = await _httpService.UpdateEntityAsync($"{_url}", userInJson);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            var userDto = JsonConvert.DeserializeObject<UserDTO>(stringResponse);

            return _mapper.Map<Models.User>(userDto);
        }


        public async Task<int> DeleteEntityAsync(int entityId)
        {
            var httpResponse = await _httpService.DeleteEntityAsync($"{_url}", entityId);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                return int.Parse(stringResponse);
            else
                return 0;
        }
    }
}
