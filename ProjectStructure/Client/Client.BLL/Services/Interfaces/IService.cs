﻿using System.Threading.Tasks;
using System.Collections.Generic;

namespace Client.BLL.Services.Interfaces
{
    public interface IService<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAllEntitiesAsync();

        Task<TEntity> GetEntityAsync(int id);

        Task<TEntity> AddEntityAsync(TEntity entity);

        Task<TEntity> UpdateEntityAsync(TEntity entity);

        Task<int> DeleteEntityAsync(int entityId);
    }
}
