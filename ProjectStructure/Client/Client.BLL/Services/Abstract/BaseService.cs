﻿using AutoMapper;

namespace Client.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        protected readonly HttpService _httpService;
        protected readonly IMapper _mapper;

        public string Host { get; }


        protected BaseService(string host, HttpService httpService)
        {
            Host = host;

            _httpService = httpService;

            var mapperConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new MappingProfile());
            });

            _mapper = mapperConfig.CreateMapper();
        }
    }
}
